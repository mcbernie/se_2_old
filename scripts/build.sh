#!/bin/bash


toplevel=$(git rev-parse --show-toplevel)
currentdir=$(pwd)
cd $toplevel
cargo build --features gl-wayland
cd $currentdir