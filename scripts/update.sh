#!/bin/bash

toplevel=$(git rev-parse --show-toplevel)

systemctl stop se.service
$toplevel/scripts/update-git.sh
$toplevel/scripts/build.sh
systemctl start se.service