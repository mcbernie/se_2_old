#[macro_use]
extern crate log;

extern crate resources;
extern crate glutin;
extern crate gl;
extern crate slideshow;
extern crate base;
extern crate background;
extern crate gui;
extern crate sprite;
extern crate client;
extern crate uuid;
extern crate gl_render;
extern crate themes;

use base::Application;


use client::{NetworkComm, Configuration};

use slideshow::Slideshow;
use background::Background;
use gui::{Gui, GuiShow};

use std::thread;
use std::time::Instant;

const TARGET_FRAME_TIME: f32 = 60.0 / 1000.0;//ms // 33.3 ms for 30 fps

use std::fmt;
use dotenv::dotenv;

#[allow(dead_code)]
enum LoadingState {
    Unknown,
    InitNetwork,
    DoLogin,
    NewKey(String),
    Finished,
    Error(String)
}

impl fmt::Display for LoadingState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        
        let printout = match &self {
            LoadingState::DoLogin => "melde am Server an".to_string(),
            LoadingState::Unknown => "initialisiere Netzwerk".to_string(),
            LoadingState::InitNetwork => "starte Netzwerk".to_string(),
            LoadingState::NewKey(key) => format!("System ist nicht registriert... {:}", key),
            LoadingState::Finished => "Netzwerk initialisierung abgeschlossen".to_string(),
            LoadingState::Error(msg) => format!("Fehler bei Netzwerk initalisierung! {}",msg),
        };

        write!(f, "{}", printout)
    }
}

#[allow(dead_code)]
enum AppState {
    Loading(LoadingState),
    BetweenLoadingAndSlideshow(String),
    NewLocation(String),
    Slideshow,
    Error,
}

struct App {
    styles: themes::Themes,
    network_comm: NetworkComm,
    slideshow: Slideshow,
    background: Background,
    gui: Gui,
    state: AppState,
    time_start: Instant,
    frame_rate_insant: Instant,
    prev_time: f32,
    show_fps: bool,
    enable_frame_limiter: bool,
    config: Configuration,
    //texture_manager: TextureManager,
}

impl App {

    // FPS System Update && Network Code Update
    fn update(&mut self, frame_time: f32) {
        self.gui.update_fps(frame_time);
    }

    fn reset_time(&mut self) {
        self.time_start = Instant::now();
    }
}



impl Application for App {
    fn new(gl: &gl::Gl, window_context: &glutin::ContextWrapper<glutin::PossiblyCurrent, glutin::Window>) -> App {
        dotenv().ok();
        let _ = ::env_logger::init();
        
        #[cfg(target_os = "linux")]
        window_context.window().hide_cursor(true);

        let styles = themes::Themes::load();

        themes::styles::test_style_loading_from_json();

        let uuid = uuid::Uuid::parse_str("3eafefc3-d548-4cf3-9476-012545cb96b3").unwrap();

        info!("start SE [{}]", uuid.to_string());

        // init network
        let network_comm = client::NetworkComm::new(uuid, "http://192.168.0.80:50051", styles.clone());
        //let network_comm = client::NetworkComm::new(uuid, "http://127.0.0.1:50051");



        let background = Background::new(gl);

        let mut slideshow = Slideshow::new(gl, window_context);

        slideshow.load_default_items(window_context);

        
        

        let gui = Gui::new(window_context, styles.clone());

        //let loader = slideshow.get_item_loader_fn();
        //let scrolltext_updater = gui.get_scrolltext_update_fn();


        /*thread::spawn(move || {
            thread::sleep(std::time::Duration::from_secs(8));
            loader("./resources/images/default2.jpg".to_string())
        });

        thread::spawn(move || {
            thread::sleep(std::time::Duration::from_secs(12));
            debug!("Timer says: New Slideshow text ->");
            scrolltext_updater("- Star Entertainer - Neue Version, noch mehr action! der Star Entertainer wird Erwachsen! -- klein, schnell, einfach...".to_string());
        });*/

        App {
            network_comm,
            
            slideshow,
            background,
            gui,
            state: AppState::Loading(LoadingState::Unknown),
            time_start: Instant::now(),
            // FPS
            frame_rate_insant: Instant::now(),
            prev_time: 0.0,
            show_fps: false,
            enable_frame_limiter: false,
            config: Configuration::default(),
            styles,
        }
    }

    fn render(&mut self, gl: &gl::Gl, window_context: &glutin::ContextWrapper<glutin::PossiblyCurrent, glutin::Window>) {
        let delta = self.time_start.elapsed();
        let delta = delta.as_secs() as f32 + delta.subsec_nanos() as f32 / 1000_000_000.0;
        let dt = {
            let elapsed = get_elapsed(&self.frame_rate_insant);
            let delta_time = elapsed - self.prev_time;
            self.prev_time = elapsed;
            delta_time
        };
        let this_frame = Instant::now();
        
        {
            let window = window_context.window();
            if let Some(display) = window.get_inner_size() {
                let dpi = window.get_hidpi_factor();
                unsafe {
                    gl.Viewport(0,0,(display.width * dpi) as i32, (display.height * dpi) as i32);
                }
            }
        }
        

        self.update(dt);

        match &self.state {
            AppState::Slideshow => {},
            _ => {self.background.render(dt);}
        };
        
        
        // Make an Background here
        match &self.state {
            AppState::Loading(loading_state) => {
                // Show loading
                self.gui.render(GuiShow::Starting(loading_state.to_string()), None, delta);
                
                match loading_state {
                    LoadingState::Finished => {
                        self.state = AppState::BetweenLoadingAndSlideshow(loading_state.to_string());
                        self.reset_time();
                    },
                    _ => {}
                };
                
            },
            AppState::BetweenLoadingAndSlideshow(text) => {
                self.gui.render(GuiShow::Starting(text.to_string()),None, delta);
                if delta > 4.0 {
                    self.state = AppState::Slideshow;
                    self.reset_time();
                } 
            }
            AppState::Slideshow => {
                
                self.slideshow.update(dt);
                self.slideshow.render();
                
                let icon = match self.network_comm.get_state() {
                    /*client::SystemState::ConnectionError | 
                    client::SystemState::Connecting(_) |
                    client::SystemState::Start |*/
                    client::SystemState::Online(_) => None,
                    _ => Some("\u{1F517}"),
                };
                self.gui.render(GuiShow::Scrolltext, icon, dt);
            },
            AppState::Error => {

            },
            AppState::NewLocation(key) => {
                self.gui.render(GuiShow::StartingNew(key.to_string()), None, delta)
            },
        }
        
        if self.show_fps == true {
            self.gui.render(GuiShow::Fps, None, dt);
        }
        
        if self.enable_frame_limiter == true {
            let fd = get_elapsed(&this_frame);
            if fd < TARGET_FRAME_TIME {
                let v = (TARGET_FRAME_TIME - fd) * 1000.0;
                thread::sleep(std::time::Duration::from_millis( v as u64)   );
            } 
        }
    }

    fn keypress(&mut self, key: glutin::VirtualKeyCode) {

        match key {
            glutin::VirtualKeyCode::F => {
                self.show_fps = !self.show_fps;
            },

            glutin::VirtualKeyCode::L => {
                self.enable_frame_limiter = !self.enable_frame_limiter;
            },
            all_other_keys => {
                self.slideshow.keypress(all_other_keys);
            }
        };

    }

    fn resize(&mut self, width: f32, height: f32, dpi: f32) {
        self.gui.resize(width, height, dpi);
        self.slideshow.resize(width, height, dpi);
    }



    fn fixed_update(&mut self, _duration: u128, window_context: &glutin::ContextWrapper<glutin::PossiblyCurrent, glutin::Window>) {
        let comm_state = self.network_comm.update();
        use client::{SystemState, ConnectingState,OnlineState};

        if let SystemState::Online(OnlineState::GotConfiguration(config)) = &comm_state {
            info!("Receive a new configuration: {:?}", config);

            //self.styles.get().write().unwrap().scrolltext.text_alfa = 128;
            let f_get = self.styles.get();
            let mut f = f_get.write().unwrap();
            use std::mem;
            mem::replace(&mut f.scrolltext, themes::styles::load_styles_from_data(config.raw_styles.clone()).scrolltext);

            let new_config = config.clone();
            if new_config.scrolltext != self.config.scrolltext && self.config.scrolltext.len() > 1 {
                let f = self.gui.get_scrolltext_update_fn();
                f(new_config.scrolltext);
            }

            self.config = config.clone();



            self.gui.scrolltext.show_clock = self.config.show_clock == 1;

        }

        if let SystemState::Online(OnlineState::GotList(list)) = &comm_state {
            info!("Receive a new List: {:?}", list);
            self.slideshow.replace_items(list, window_context);
        }

        match &self.state {
            AppState::Loading(_d) => {
                self.state = match comm_state {
                    SystemState::Connecting(ConnectingState::GotRegistrationKey(key)) | 
                    SystemState::Connecting(ConnectingState::CheckForRegistrationFinished(key)) => AppState::NewLocation(key),
                    SystemState::Connecting(ConnectingState::LoginError(msg)) => AppState::Loading(LoadingState::Error(msg)),
                    SystemState::Connecting(_) => AppState::Loading(LoadingState::DoLogin),
                    SystemState::Online(_) => AppState::Loading(LoadingState::Finished),
                    _ => AppState::Loading(LoadingState::Unknown),
                };
            },
            AppState::NewLocation(_key) => {
                match comm_state {
                    SystemState::Connecting(ConnectingState::GotRegistrationKey(_key)) | 
                    SystemState::Connecting(ConnectingState::CheckForRegistrationFinished(_key)) => {},
                    _ => {
                        self.state = AppState::Loading(LoadingState::DoLogin);
                    },
                };
            },
            _ => {
                match comm_state {
                    SystemState::Connecting(ConnectingState::GotRegistrationKey(key)) => {
                        self.state = AppState::NewLocation(key);
                    },
                    _ => {},
                };
            }
        };

    }


}

fn get_elapsed(instant: &Instant) -> f32 {
    let elapsed = instant.elapsed();
    let elapsed = elapsed.as_secs() as f64 + elapsed.subsec_nanos() as f64 * 1e-9;
    elapsed as f32
}

fn main() {
    use base::Application;
    App::run("NOIS");
}
