#!bin/bash

#echo "remove dev packages"
#apt-get remove -y \
#    libglfw3-dev libgles2-mesa-dev libgbm-dev libinput-dev \
#    libxkbcommon-dev libpixman-1-dev libcap-dev libpng-dev \
#    libxcb-xinput-dev libxcb-composite0-dev libavutil-dev \
#    libavcodec-dev libavformat-dev libsystemd-dev \
#    libcairo2-dev libwebp-dev libjpeg-dev libdbus-1-dev libpam0g-dev


echo "remove gnome stuff"
apt autoremove --purge snapd
apt-get remove gnome-shell 
apt-get purge --auto-remove ubuntu-gnome-desktop
apt-get autoremove 
dpkg-reconfigure gdm
apt-get remove gdm  
apt-get autoremove --purge cups-server-common cups-daemon
