#!bin/bash

WESTON_VERSION=6.0
WESTON_URL=https://github.com/wayland-project/weston.git
SYSTEM_USER_NAME=setest

echo "Install Required Packages"
sudo ./helper/prepare.sh

currentdir=$(pwd)

mkdir -p /tmp/weston_build && cd /tmp/weston_build

echo "get Weston from $WESTON_URL and checkout version $WESTON_VERSION"
git clone $WESTON_URL && cd weston && git checkout $WESTON_VERSION

echo "build weston"
meson build/ --prefix="/usr" \
    -Dshell-fullscreen=true -Dbackend-rdp=false \
    -Dxwayland=false -Dcolor-management-lcms=false \
    -Dcolor-management-colord=false -Ddemo-clients=false \
    -Dbackend-default='drm' -Dsimple-dmabuf-drm='intel' \
    -Dtest-junit-xml=false

ninja -C build/ install


echo "Install Weston Configuration"
sudo mkdir -p /etc/weston && cp weston.ini /etc/weston/weston.ini

echo "install weston service && enable graphical target"
cp weston.service /etc/systemd/system/weston@.service
sudo systemctl set-default graphical.target
systemctl enable weston@.service
sudo systemctl daemon-reload

echo "Install PAM Stack"
cp weston.pam /etc/pam.d/weston


echo "Add Groups"
groupadd -r autologin
gpasswd -a $SYSTEM_USER_NAME autologin


echo "Cleanup"
sudo ./helper/clean.sh
rm -rf /tmp/weston_build
cd $currentdir
