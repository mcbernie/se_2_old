#[macro_use]
extern crate log;



extern crate slideshow;
extern crate resources;
extern crate glutin;
extern crate gl;
extern crate base;
extern crate background;
extern crate gui;
extern crate sprite;

use base::Application;

use background::Background;
use gui::{Gui, GuiShow};

use slideshow::{Slideshow};
use slideshow::things::Statistic;

use std::thread;
use std::time::Instant;

const TARGET_FRAME_TIME: f32 = 60.0 / 1000.0;//ms // 33.3 ms for 30 fps

use std::fmt;
use dotenv::dotenv;

struct App {
    slideshow: Slideshow,
    time_start: Instant,
    frame_rate_insant: Instant,
    prev_time: f32,
    show_fps: bool,
    enable_frame_limiter: bool,
}

impl App {

    // FPS System Update && Network Code Update
    fn update(&mut self, frame_time: f32) {
        //self.gui.update_fps(frame_time);
    }

    fn reset_time(&mut self) {
        self.time_start = Instant::now();
    }
}



impl Application for App {
    fn new(gl: &gl::Gl, window_context: &glutin::ContextWrapper<glutin::PossiblyCurrent, glutin::Window>) -> App {
        dotenv().ok();
        let _ = ::env_logger::init();

        //let background = Background::new(gl);

        #[cfg(target_os = "linux")]
        window_context.window().hide_cursor(true);


        //let mut gui = Gui::new(window_context);

        let mut slideshow = Slideshow::new(gl, window_context);
        
        slideshow.load_statistics();

        App {
            slideshow,
            time_start: Instant::now(),
            // FPS
            frame_rate_insant: Instant::now(),
            prev_time: 0.0,
            show_fps: false,
            enable_frame_limiter: false,
        }
    }

    fn render(&mut self, gl: &gl::Gl, window_context: &glutin::ContextWrapper<glutin::PossiblyCurrent, glutin::Window>) {
        let delta = self.time_start.elapsed();
        let delta = delta.as_secs() as f32 + delta.subsec_nanos() as f32 / 1000_000_000.0;
        let dt = {
            let elapsed = get_elapsed(&self.frame_rate_insant);
            let delta_time = elapsed - self.prev_time;
            self.prev_time = elapsed;
            delta_time
        };
        let this_frame = Instant::now();
        
        {
            let window = window_context.window();
            if let Some(display) = window.get_inner_size() {
                let dpi = window.get_hidpi_factor();
                unsafe {
                    gl.Viewport(0,0,(display.width * dpi) as i32, (display.height * dpi) as i32);
                }
            }
        }
        

        self.update(dt);
        self.slideshow.update(dt);

        self.slideshow.render();
        
        //self.background.render(dt);
        //self.gui.render(GuiShow::Scrolltext, None, dt);
            
        /*if self.show_fps == true {
            self.gui.render(GuiShow::Fps, None, dt);
        }*/
        
        if self.enable_frame_limiter == true {
            let fd = get_elapsed(&this_frame);
            if fd < TARGET_FRAME_TIME {
                let v = (TARGET_FRAME_TIME - fd) * 1000.0;
                thread::sleep(std::time::Duration::from_millis( v as u64)   );
            } 
        }
    }

    fn keypress(&mut self, key: glutin::VirtualKeyCode) {

        match key {
            glutin::VirtualKeyCode::F => {
                self.show_fps = !self.show_fps;
            },

            glutin::VirtualKeyCode::L => {
                self.enable_frame_limiter = !self.enable_frame_limiter;
            },

            
            _ => {}

        };

    }

    fn resize(&mut self, width: f32, height: f32, dpi: f32) {
        //self.gui.resize(width, height, dpi);
        self.slideshow.resize(width, height, dpi);
    }

    fn fixed_update(&mut self, _duration: u128) {
    }

}

fn get_elapsed(instant: &Instant) -> f32 {
    let elapsed = instant.elapsed();
    let elapsed = elapsed.as_secs() as f64 + elapsed.subsec_nanos() as f64 * 1e-9;
    elapsed as f32
}

fn main() {
    use base::Application;
    App::run("Star Entertainer System");
}
