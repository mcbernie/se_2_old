
use nanovg;
use nanovg::{Color, TextOptions, Alignment, Font};


pub const CHAIN_ICON: &str = "\u{1F517}";


pub struct Infos{
}


impl Infos {
    pub fn new(_context: &nanovg::Context) -> Infos {
        Infos {}
    }

    pub fn render(&mut self, context: &nanovg::Context, width: f32, height: f32, dpi: f32, _dt: f32, icon: &'static str) {
        let f = Font::find(context, "Entypo").unwrap();
        context.frame((width, height), dpi, |frame| {
            frame.text(f, 
                (width - 10.0, 0.0), 
                icon, 
                TextOptions {
                    color: Color::from_rgba(255, 255, 125, 255),
                    size: 40.0,
                    align: Alignment::new().right().top(),
                    ..Default::default()
                },
            );
        });
    }
}