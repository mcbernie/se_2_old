
use nanovg::{Color, Gradient, PathOptions, TextOptions, Alignment, Font, Context, Frame, Direction, Winding, Transform};


use std::f32::consts;

pub fn render_starting(ctx: &Context, width: f32, height: f32, dpi: f32, dt: f32, custom_text: String) {

    let f = Font::find(ctx, "Monserrat-Bold").unwrap();

    ctx.frame((width, height), dpi, |mut frame| {
        let scale_width =  width / 1920.0;
        let scale_height = height / 1080.0;
        let width = 1920.0;
        let height = 1080.0;

        frame.transformed(Transform::new().scale(scale_width, scale_height), |frame| {
            frame.text(
                f,
                (width / 2.0 , (height / 2.0) - 50.0),
                "Star Entertainer wird gestartet",
                TextOptions {
                    color: Color::new(1.0, 1.0, 1.0, 1.0),
                    size: 30.0,
                    align: Alignment::new().center(),
                    ..Default::default()
                },
            );
            frame.text(
                f,
                (width / 2.0 , (height / 2.0) - 20.0),
                custom_text,
                TextOptions {
                    color: Color::new(1.0, 1.0, 1.0, 1.0),
                    size: 20.0,
                    //letter_spacing: (dt * 0.1) * 30.0,
                    align: Alignment::new().center(),
                    ..Default::default()
                },
            );
            draw_spinner(&frame, PathOptions::default(), width / 2.0, (height / 2.0) + 80.0, 40.0, dt * 0.95);
        });
    });
}

pub fn render_starting_new(ctx: &Context, width: f32, height: f32, dpi: f32, dt: f32, key: String) {



    let f = Font::find(ctx, "Monserrat-Bold").unwrap();
    let f_n = Font::find(ctx, "Gidole").unwrap();
    
    
    ctx.frame((width, height), dpi, |mut frame| {
        let scale_width =  width / 1920.0;
        let scale_height = height / 1080.0;
        let width = 1920.0;
        let height = 1080.0;

        frame.transformed(Transform::new().scale(scale_width, scale_height), |mut frame| {
            frame.text(
                f,
                (width / 2.0 , 20.0),
                "Star Entertainer - Einrichtung",
                TextOptions {
                    color: Color::new(1.0, 1.0, 1.0, 1.0),
                    size: 30.0,
                    align: Alignment::new().center(),
                    ..Default::default()
                },
            );

            

            let mut y = 160.0;
            let x = 0.0;

            let box_text_options = TextOptions {
                color: Color::new(1.0, 1.0, 1.0, 1.0),
                size: 22.0,
                //letter_spacing: (dt * 0.1) * 30.0,
                align: Alignment::new().left(),
                line_height: 1.2,
                line_max_width: width / 1.2,
                ..Default::default()
            };
            let info_text = "Dieser Star Entertainer ist dem System nicht bekannt!\n  Um die Einrichtung abzuschließen geben sie den unten genannten Key im Backend ein.";
            let bounds = frame.text_box_bounds(f_n, (0.0,y), info_text, box_text_options);

            frame.transformed(Transform::new().translate(width / 2.0 - bounds.max_x / 2.0, 0.0), |frame| {

                frame.path(
                    |path| {
                        path.rounded_rect(
                            (bounds.min_x - 5.0 , bounds.min_y - 5.0 ),
                            ((bounds.max_x - bounds.min_x) + 10.0 , (bounds.max_y - bounds.min_y) + 10.0),
                            3.0,
                        );
                        let px = (bounds.max_x + bounds.min_x) / 2.0;
                        let py = bounds.max_y + 5.0;
                        path.move_to((px, py + 10.0));
                        path.line_to((px - 7.0, py - 1.0));
                        path.line_to((px + 7.0, py - 1.0));
                        path.fill(Color::from_rgba(145, 153, 153, 200), Default::default());
                    },
                    PathOptions {
                        ..Default::default()
                    }
                );

                frame.text_box(
                    f_n,
                    (x,y),
                    info_text,
                    box_text_options
                );
            });
            

            y = bounds.max_y + 40.0;
            frame.text(
                f_n,
                (width / 2.0,y),
                key,
                TextOptions {
                    color: Color::new(1.0, 1.0, 1.0, 1.0),
                    size: 40.0,
                    //letter_spacing: (dt * 0.1) * 30.0,
                    align: Alignment::new().center().middle(),
                    ..Default::default()
                },
            );
            draw_spinner(&frame, PathOptions::default(), width / 2.0, (height / 2.0) + 80.0, 40.0, dt * 0.95);
        });
    });
}




fn draw_spinner(frame: &Frame, options: PathOptions, cx: f32, cy: f32, r: f32, t: f32) {
    let a0 = 0.0 + t * 6.0;
    let a1 = consts::PI + t * 6.0;
    let r0 = r;
    let r1 = r * 0.75;

    frame.path(
        |path| {
            let ax = cx + f32::cos(a0) * (r0 + r1) * 0.5;
            let ay = cy + f32::sin(a0) * (r0 + r1) * 0.5;
            let bx = cx + f32::cos(a1) * (r0 + r1) * 0.5;
            let by = cy + f32::sin(a1) * (r0 + r1) * 0.5;
            path.arc((cx, cy), r0, a0, a1, Winding::Direction(Direction::Clockwise));
            path.arc((cx, cy), r1, a1, a0, Winding::Direction(Direction::CounterClockwise));
            path.fill(
                Gradient::Linear {
                    start: (ax, ay),
                    end: (bx, by),
                    start_color: Color::from_rgba(255, 255, 255, 0),
                    end_color: Color::from_rgba(255, 255, 255, 170)
                },
                Default::default()
            );
        },
        options
    );
}