
use nanovg::{Alignment, Color, Font, Frame, TextOptions};

const GRAPH_HISTORY_COUNT: usize = 100;

#[allow(dead_code)]
pub enum GraphRenderStyle {
    Fps,
    Ms,
    Percent
}

pub struct PerformanceGraph {
    style: GraphRenderStyle,
    name: String,
    values: [f32; GRAPH_HISTORY_COUNT],
    head: usize
}

impl PerformanceGraph {
    pub fn new(style: GraphRenderStyle, name: &str) -> PerformanceGraph {
        PerformanceGraph {
            style,
            name: String::from(name),
            values: [0.0; GRAPH_HISTORY_COUNT],
            head: 0,
        }
    }

    pub fn update(&mut self, frame_time: f32) {
        self.head = (self.head + 1) % GRAPH_HISTORY_COUNT;
        self.values[self.head] = frame_time;
    }

    pub fn draw(&self, frame: &Frame, font: Font, x: f32, y: f32) {
        let w = 200.0;
        let h = 35.0;
        let average = self.average();

        frame.path(
            |path| {
                path.rect((x, y), (w, h));
                path.fill(Color::from_rgba(0, 0, 0, 128), Default::default());
            },
            Default::default()
        );

        frame.path(
            |path| {
                path.move_to((x, y + h));
                match self.style {
                    GraphRenderStyle::Fps => {
                        for i in 0..self.values.len() {
                            let v = 1.0 / (0.00001 + self.values[(self.head + i) % self.values.len()]);
                            let v = clamp(v, 0.0, 80.0);
                            let vx = x + (i as f32 / (self.values.len() - 1) as f32) * w;
                            let vy = y + h - ((v / 80.0) * h);
                            path.line_to((vx, vy));
                        }
                    },
                    GraphRenderStyle::Ms => {
                        for i in 0..self.values.len() {
                            let v = self.values[(self.head + i) % self.values.len()] * 1000.0;
                            let v = clamp(v, 0.0, 20.0);
                            let vx = x + (i as f32 / (self.values.len() - 1) as f32) * w;
                            let vy = y + h - ((v / 20.0) * h);
                            path.line_to((vx, vy));
                        }
                    },
                    GraphRenderStyle::Percent => {
                        for i in 0..self.values.len() {
                            let v = self.values[(self.head + i) % self.values.len()] * 1.0;
                            let v = clamp(v, 0.0, 100.0);
                            let vx = x + (i as f32 / (self.values.len() - 1) as f32) * w;
                            let vy = y + h - ((v / 100.0) * h);
                            path.line_to((vx, vy));
                        }
                    }
                }

                path.line_to((x + w, y + h));

                path.fill(Color::from_rgba(255, 192, 0, 128), Default::default());
            },
            Default::default()
        );

        frame.text(font, (x + 3.0, y + 1.0), &self.name, TextOptions {
            color: Color::from_rgba(240, 240, 240, 192),
            align: Alignment::new().left().top(),
            size: 14.0,
            ..Default::default()
        });

        match self.style {
            GraphRenderStyle::Fps => {
                frame.text(
                    font,
                    (x + w - 3.0, y + 1.0),
                    format!("{:.2} FPS", 1.0 / average),
                    TextOptions {
                        size: 18.0,
                        color: Color::from_rgba(240, 240, 240, 255),
                        align: Alignment::new().right().top(),
                        ..Default::default()
                    }
                );

                frame.text(
                    font,
                    (x + w - 3.0, y + h - 1.0),
                    format!("{:.2} ms", average * 1000.0),
                    TextOptions {
                        size: 15.0,
                        color: Color::from_rgba(240, 240, 240, 160),
                        align: Alignment::new().right().bottom(),
                        ..Default::default()
                    }
                );
            },
            GraphRenderStyle::Ms => {
                frame.text(
                    font,
                    (x + w - 3.0, y + 1.0),
                    format!("{:.2} ms", average * 1000.0),
                    TextOptions {
                        size: 18.0,
                        color: Color::from_rgba(240, 240, 240, 255),
                        align: Alignment::new().right().top(),
                        ..Default::default()
                    }
                );
            },
            GraphRenderStyle::Percent => {
                frame.text(
                    font,
                    (x + w - 3.0, y + 1.0),
                    format!("{:.1} %", average * 1.0),
                    TextOptions {
                        size: 18.0,
                        color: Color::from_rgba(240, 240, 240, 255),
                        align: Alignment::new().right().top(),
                        ..Default::default()
                    }
                )
            }
        }
    }

    pub fn average(&self) -> f32 {
        self.values.iter().sum::<f32>() / self.values.len() as f32
    }
}


fn clamp(value: f32, min: f32, max: f32) -> f32 {
    if value < min {
        min
    } else if value > max {
        max
    } else {
        value
    }
}
