use nanovg;

use std::thread;
use std::sync::mpsc;
use std::sync::mpsc::{Sender, Receiver};

use themes;

use nanovg::{Color, Clip, Gradient, Scissor, TextOptions, Alignment, Font, Transform, PathOptions, StrokeOptions};

use chrono::{DateTime, Local};

const SPACE_BETWEEN_TEXT: f32 = 40.0;



pub struct Scrolltext{
    text: String,
    tx: Sender<String>,
    rx: Receiver<String>,
    styles: themes::Themes,
    x: f32,
    pub show_clock: bool,
    pub enable_border: bool,
}


impl Scrolltext {
    pub fn new(styles: themes::Themes) -> Scrolltext {
        //let text = "Star Entertainer - Super neu und unfassbar toll! Manchmal können Frösche fliegen und ganz viele Kinder kriegen... Viele Witze kenne ich nur der eine der Fliegt nicht. Affen können einen Kuss, dann ist mit dem Leben.".to_string();
        let text = "Star Entertainer - Super neu und unfassbar toll!".to_string();


        let (tx, rx) = mpsc::channel();

        Scrolltext {
            text,
            tx,
            rx,
            x: 0.0,
            show_clock: true,
            enable_border: true,
            styles,
        }
    }

    pub fn get_update_fn(&mut self) -> impl Fn(String) {
        let tx = self.tx.clone();

        move |text: String| {

            let tx = tx.clone();

            thread::spawn(move || {
                tx.send(text).unwrap();
            });


        }
        
    }

    pub fn resize(&mut self, _width: f32, _height: f32, _dpi: f32) {
        
    }

    pub fn update_text(&mut self, text: String) {
        self.text = text;
    }

    pub fn set_border(&mut self, enable: bool) {
        self.enable_border = enable;
    }

    pub fn render(&mut self, context: &nanovg::Context, width: f32, height: f32, dpi: f32, dt: f32) {
        {
            let received_new_text = self.rx.try_recv();
            if received_new_text.is_ok() {
                self.update_text(received_new_text.unwrap());
            }
        }
        let get_styles = self.styles.get();
        let style = &get_styles.read().unwrap().scrolltext;


        let s = style.scrolltext_speed * dt;
        self.x -= s;


        let f = Font::find(context, "Gidole").unwrap();
    
        context.frame((width, height), dpi, |mut frame| {
            let scale_width =  width / 1920.0;
            let scale_height = height / 1080.0;
            let width = 1920.0;
            let height = 1080.0;

            frame.transformed(Transform::new().scale(scale_width, scale_height), |frame| {


                let left_side = if self.show_clock {
                    style.clock_width
                } else {
                    0.0
                };


                let scissor = Scissor {
                    x: left_side + style.padding_left,
                    y: height - (style.box_height + style.padding_bottom),
                    width: width - (style.padding_right + style.padding_left + left_side),
                    height: style.box_height,
                    transform: None,
                };

                let text_options = TextOptions {
                    color: Color::from_rgba(style.text_color[0], style.text_color[1], style.text_color[2], style.text_alfa),
                    size: style.text_size,
                    align: Alignment::new().left().middle(),
                    clip: Clip::Scissor(scissor),
                    blur: 0.0,
                    ..Default::default()
                };
                let shadow_text_options = TextOptions {
                    color: Color::from_rgba(style.text_border_color[0], style.text_border_color[1], style.text_border_color[2], style.text_border_alfa),
                    size: style.text_size,
                    align: Alignment::new().left().middle(),
                    clip: Clip::Scissor(scissor),
                    blur: 0.5,
                    ..Default::default()
                };
                

                frame.path(
                    |path| {
                        path.rect(
                            (left_side + style.padding_left, height - (style.box_height + style.padding_bottom)), 
                            (width - (style.padding_right + left_side + style.padding_left), style.box_height)
                        );
                        path.fill(
                            Gradient::Linear {
                                start: (0.0, height - (style.box_height + style.padding_bottom)),
                                end: (0.0, height - style.padding_bottom + 10.0),
                                start_color: Color::from_rgba(style.background_start_color[0], style.background_start_color[1], style.background_start_color[2], style.background_alfa),
                                end_color:   Color::from_rgba(style.background_end_color[0], style.background_end_color[1], style.background_end_color[2], style.background_alfa),
                            },
                            Default::default()
                        );
                    },
                    PathOptions{
                        //alpha: 0.5,
                        ..Default::default()
                    }
                );

                let (_a, m) = frame.text_bounds(f, ( 0.0, 0.0 ),&self.text, text_options);
                

                let size = m.max_x + m.min_x + SPACE_BETWEEN_TEXT;
                let box_width = width - (style.padding_left + style.padding_right + left_side);


                //println!("M: {:?} A:{:?}, size: {}, box_width: {}, count: {}, x: {}", m, a, size, box_width, count, self.x);
                let mut position = 0.0;
                while position <= box_width + size {
                    if self.enable_border == true {                    
                        frame.text_box(
                            f,
                            ( (position + self.x + style.padding_left + left_side) - 0.5, ((height - (style.box_height + style.padding_bottom)) + style.box_height / 2.0) - 0.5 ),
                            &self.text,
                            shadow_text_options
                        );
                        frame.text_box(
                            f,
                            ( (position + self.x + style.padding_left + left_side) + 0.5, ((height - (style.box_height + style.padding_bottom)) + style.box_height / 2.0) + 0.5 ),
                            &self.text,
                            shadow_text_options
                        );
                    }
                    frame.text_box(
                        f,
                        ( position + self.x + style.padding_left + left_side, (height - (style.box_height + style.padding_bottom)) + style.box_height / 2.0 ),
                        &self.text,
                        text_options
                    );
                    position += size;
                    
                }
                if self.x < 0.0 - size {
                    self.x = 0.0;
                }
                

                if self.show_clock {
                    frame.path(
                        |path| {
                            path.rect(
                                (style.padding_left, height - (style.box_height + style.padding_bottom)), 
                                (style.clock_width, style.box_height)
                            );
                            path.fill(
                                Color::from_rgba(style.clock_background_color[0], style.clock_background_color[1], style.clock_background_color[2], style.clock_background_alfa),
                                Default::default()
                            );
                        },
                        PathOptions{
                            ..Default::default()
                        }
                    );

                    let now: DateTime<Local> = Local::now();
                    frame.text(
                        f,
                        ( style.padding_left + (style.clock_width / 2.0), (height - (style.box_height + style.padding_bottom)) + style.box_height / 2.0 ),
                        format!("{}", now.format("%H:%M:%S")),
                        TextOptions {
                            color: Color::from_rgba(style.clock_color[0], style.clock_color[1], style.clock_color[2], style.clock_alfa),
                            size: style.text_size,
                            align: Alignment::new().center().middle(),
                            ..Default::default()
                        }
                    );
                }

                // now draw a line over all things
                frame.path(|path| {
                    path.move_to((style.padding_left, height - (style.box_height + style.padding_bottom)));
                    path.line_to((width - style.padding_right, height - (style.box_height + style.padding_bottom)));
                    path.stroke(
                        Color::from_rgba(style.stroke_on_top_color[0],style.stroke_on_top_color[1],style.stroke_on_top_color[2], style.stroke_on_top_alfa),
                        StrokeOptions {
                            width: style.stroke_on_top_width,
                            antialias: false,
                            ..Default::default()
                        }
                    );

                }, 
                PathOptions{
                    ..Default::default()
                });

            });


            
        });

        

    }


}
