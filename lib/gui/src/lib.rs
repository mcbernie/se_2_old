extern crate gl;
extern crate glutin;
extern crate nanovg;
extern crate chrono;
extern crate themes;

use nanovg::Font;

pub mod contents;

pub enum GuiShow {
    Starting(String),
    StartingNew(String),
    Debug,
    Scrolltext,
    Fps,
}

pub struct Gui {
    context: nanovg::Context,
    width: f32,
    height: f32,
    dpi: f32,
    pub scrolltext: contents::Scrolltext,
    graph: contents::graph::PerformanceGraph,
    infos: contents::Infos,
    styles: themes::Themes,
}



impl Gui {

    fn create_nanovg_context() -> nanovg::Context {
        nanovg::ContextBuilder::new().build().expect("Initialization of NanoVG failed!")
    }

    pub fn new(window_context: &glutin::ContextWrapper<glutin::PossiblyCurrent, glutin::Window>, styles: themes::Themes) -> Gui {
        let context = Self::create_nanovg_context();

        Font::from_file(&context, "Mechanic", "resources/Mechanic of the Heart.ttf")
            .expect("Faile to load font 'Mechanic of the Heart.ttf'");

        Font::from_file(&context, "Monserrat-Bold", "resources/Montserrat-Bold.ttf")
            .expect("Faile to load font 'Montserrat-Bold.otf'");            

        Font::from_file(&context, "Entypo", "resources/entypo.ttf")
            .expect("Failed to load font 'entypo.ttf'");
        
        Font::from_file(&context, "Gidole", "resources/gidole-regular.ttf")
            .expect("Failed to load font 'gidole-Regular.ttf'");

        Font::from_file(&context, "Sidewalk", "resources/sidewalk.ttf")
            .expect("Failed to load font 'sidewalk.ttf'");

        let (width, height, dpi) = {
            let window = window_context.window();
            let dpi_factor = window.get_hidpi_factor();
            let width = 800.0;
            let height = 600.0;
            (width, height, dpi_factor as f32)
        };

        let scrolltext = contents::Scrolltext::new(styles.clone());
        let graph = contents::graph::PerformanceGraph::new(contents::graph::GraphRenderStyle::Fps, "FPS Counter");
        let infos = contents::Infos::new(&context);

        Gui {
            context,
            width,
            height,
            dpi,
            scrolltext,
            graph,
            infos,
            styles: styles,
        }
    }

    pub fn update_fps(&mut self, frame_time: f32) {
        self.graph.update(frame_time);
    }

    pub fn get_scrolltext_update_fn(&mut self) -> impl Fn(String) {
        self.scrolltext.get_update_fn()
    }

    


    pub fn render(&mut self, show: GuiShow, info: Option<&'static str>, dt: f32) {
        
        if let Some(icon) = info {
            self.infos.render(&self.context, self.width, self.height, self.dpi, dt, icon);
        }

        match show {
            GuiShow::Starting(text) => {
                contents::render_starting(&self.context, self.width, self.height, self.dpi, dt, text)
            },
            GuiShow::Debug => {

            },
            GuiShow::Scrolltext => {
                self.scrolltext.render(&self.context, self.width, self.height, self.dpi, dt);
            },
            GuiShow::Fps => {
                let f = Font::find(&self.context, "Monserrat-Bold").unwrap();
                self.context.frame((self.width, self.height), self.dpi, |frame| {
                    self.graph.draw(&frame, f, 0.0, 0.0);
                });
            },
            GuiShow::StartingNew(key) => {
                contents::render_starting_new(&self.context, self.width, self.height, self.dpi, dt, key);
            }

        };

        
        
        
    }




    pub fn resize(&mut self, width: f32, height: f32, dpi: f32) {
        self.width = width;
        self.height = height;
        self.dpi = dpi;

        self.scrolltext.resize(width, height, dpi);
    }
}