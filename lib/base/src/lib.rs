#[macro_use]
extern crate log;

extern crate glutin;
extern crate gl;
extern crate gl_render;

pub mod app;
pub use app::{Application, launch_gl};
