use gl;
use gl_render::{load_gl, resize,clear};
use glutin;

use std::time::{Instant};

use std::ops::Range;

trait Contains<T> {
    fn contains(&self, T) -> bool;
}

impl<T: PartialOrd> Contains<T> for Range<T> {
   fn contains(&self, elt: T) -> bool { self.start >= elt && elt < self.end }
}

pub trait Application: Sized {
    fn new(&gl::Gl, &glutin::ContextWrapper<glutin::PossiblyCurrent, glutin::Window>) -> Self;
    fn render(&mut self, &gl::Gl, &glutin::ContextWrapper<glutin::PossiblyCurrent, glutin::Window>);
    
    #[allow(unused_variables)]
    fn resize(&mut self, width: f32, height: f32, dpi_factor: f32) {

    }

    fn run(title: &'static str) where Self: Application {
        launch_gl::<Self>(title);
    }

    fn keypress(&mut self, key: glutin::VirtualKeyCode);

    fn fixed_update(&mut self, duration: u128, window_context: &glutin::ContextWrapper<glutin::PossiblyCurrent, glutin::Window>);
}

pub fn launch_gl<A>(title: &'static str)
    where A: Application {

    let mut events_loop = glutin::EventsLoop::new();

    let window = glutin::WindowBuilder::new()
        .with_title(title);

    #[cfg(target_os = "linux")]
    let window = window
                    .with_fullscreen(Some(events_loop.get_primary_monitor()))
                    .with_decorations(false)
                    //.with_always_on_top(true)
                    //.with_maximized(true)
                    //.with_max_dimensions(glutin::dpi::LogicalSize::new(1280.0, 768.0)) // 1280 × 720
                    ; //

    let windowed_context = glutin::ContextBuilder::new()
        //.with_double_buffer(Some(true))
        //.with_vsync(true)
        .with_multisampling(4)
        .with_stencil_buffer(4)
        .build_windowed(window, &events_loop)
        
        .unwrap();

    let windowed_context = unsafe { windowed_context.make_current().map_err(|(_, err)| err).unwrap() };
    


    let gl = load_gl(&windowed_context);

    let mut app = A::new(&gl, &windowed_context);

    let mut running = true;

    let mut fixed_interval = Instant::now();

    while running {
        #[allow(clippy::single_match)]
        events_loop.poll_events(|event| match event {
            glutin::Event::WindowEvent { event, .. } => match event {
                glutin::WindowEvent::CloseRequested => running = false,
                glutin::WindowEvent::KeyboardInput {
                    input: glutin::KeyboardInput {
                        state: glutin::ElementState::Pressed,
                        virtual_keycode: key,
                        ..
                    },
                    ..
                } => {
                    app.keypress(key.unwrap());

                    if key == Some(glutin::VirtualKeyCode::Escape) {
                        running = false
                    }
                },
                glutin::WindowEvent::Resized(logical_size) => {
                    debug!("received window resize event: width:{}, height:{}", logical_size.width, logical_size.height);
                    let dpi_factor = windowed_context.window().get_hidpi_factor();
                    windowed_context.resize(logical_size.to_physical(dpi_factor));
                    resize(&gl, logical_size.to_physical(dpi_factor));
                    app.resize(logical_size.width as f32, logical_size.height as f32, dpi_factor as f32);
                }
                _ => (),
            },
            _ => (),
        });

        { 
            let duration = Instant::now().duration_since(fixed_interval).as_millis();
            if  duration >= 100 {
                app.fixed_update(duration, &windowed_context);
                fixed_interval = Instant::now();
            }
        }
        
        clear(&gl);
        app.render(&gl, &windowed_context);

        windowed_context.swap_buffers().unwrap();
    }
}