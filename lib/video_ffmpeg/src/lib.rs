#[macro_use]
extern crate log;
extern crate gl_render;
extern crate ffmpeg;
extern crate gl;

pub mod source;

use ffmpeg::{time};

use gl_render::Texture;

use source::video;

#[derive(Debug)]
pub enum GfxFfmpegError {
    None,
    FfmpegError(String),
}

pub struct Video {
    video: Option<video::Video>,
    video_width: u32,
    video_height: u32,
    path: String,
    play: bool,
    restart: bool,
    stopped_at: f64,
    pub texture: Option<Texture>,
}


impl Video {
    pub fn new(path: String) -> Self {
        ffmpeg::init().unwrap();
        debug!("{}", ffmpeg::format::configuration());

        Video {
            path,
            video_width: 800,
            video_height: 600,
            video: None,
            play: false,
            restart: true,
            stopped_at: 0.0,
            texture: None,
        }
    }



    pub fn load_video(&mut self, gl: &gl::Gl) -> Result<u32, GfxFfmpegError> {
        // Do some video testing
        info!("load the video...");
        let v = source::spawn_video(self.path.as_str());
        self.video = match v {
            Err(error) => {
                //println!("error: ffmpeg: {}", error);
                return Err(GfxFfmpegError::FfmpegError(error.to_string()));
            },

            Ok(v) =>
                v
        };

        let (video_width, video_height) = {
            if let Some(video) = self.video.as_ref() {
                let w = video.width() as u32;
                let h = video.height() as u32;

                info!("Video w:{:}, h:{:}", w, h);
                
                ( w, h )
            } else {
                ( 800, 600)
            }
        };

        self.video_width = video_width;
        self.video_height = video_height;

        let tex = Texture::from_empty(gl, self.video_width, self.video_height).unwrap();
        let id = tex.id();

        self.texture = Some(tex);

        return Ok(id);
    }

    pub fn update(&mut self) {
        if self.play == false {
            return;
        }

        if let Some(video) = self.video.as_mut() {
            if self.play == true {
                video.sync();
            }
            
        };

        let restart_enabled = self.restart;

        let frame = self.video.as_mut().and_then( |v| 
            if v.is_done() {
                if restart_enabled == true {
                    v.restart()
                }
                
                None
            } else {
                Some(v.frame())
            }
        );

        if let Some(f) = frame {

            //println!("d: {:?}", f.planes());
            let p = f.data(0).to_vec();
            if let Some(tex) = self.texture.as_mut() {
                use gl_render::Updateable;
                tex.update_from_buffer(p);
            }

        }
    }

    pub fn start_playing(&mut self) {
        let start = time::relative() as f64 / 1_000_000.0;

        if let Some(video) = self.video.as_mut() {
            video.start(start);
            self.play = true;
        }
    }

    pub fn play(&mut self) {
        if self.stopped_at > 0.0 {
            if let Some(video) = self.video.as_mut() {
                video.resume((time::relative() as f64 - self.stopped_at) / 1_000_000.0);
                self.stopped_at = 0.0;
                self.play = true;
            }

        }
    }

    pub fn pause(&mut self) {
        self.play = false;
        self.stopped_at = time::relative() as f64;
    }


}