#[macro_use]
extern crate log;

extern crate serde;
extern crate serde_json;
extern crate base64;

pub mod styles;

use std::sync::{Arc, Mutex, RwLock};


#[derive(Clone)]
pub struct Themes {
    inner: Arc<RwLock<styles::Styles>>,
}

impl Themes {
    pub fn load() -> Themes 
    {
        Themes {
            inner: Arc::new(RwLock::new(styles::Styles::default()))
        }
    }
    

    pub fn get(&self) -> Arc<RwLock<styles::Styles>> {
        self.inner.clone()
    }

}



