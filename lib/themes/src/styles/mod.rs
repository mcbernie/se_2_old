mod scrolltext_style;
pub use scrolltext_style::ScrolltextStyle;

use serde::{Deserialize, Serialize};

use std::str;
use base64::{encode, decode};

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Styles {
    pub scrolltext: ScrolltextStyle,
}

impl Default for Styles {
    fn default() -> Styles { 
        Styles {
            scrolltext: ScrolltextStyle::default(),
        }
    }
}

pub fn load_styles_from_data(data: String) -> Styles {
    if let Ok(style_data_bytes) = decode(&data) {
        if let Ok(style_data_string) = str::from_utf8(&style_data_bytes) {
            let styles_re: serde_json::Result<Styles> = serde_json::from_str(style_data_string);
            if let Err(e) = styles_re {
                println!("Error: {:?}", e);
                debug!("loading styles: using default, json decoding failed");
                Styles::default()
            } else {
                styles_re.unwrap()
            }
        } else {
            error!("loading styles: using default, bytes to string decoding failed");
            Styles::default()
        }
        
    } else {
        debug!("loading styles: using default, base64 decoding failed");
        Styles::default()
    }
}


pub fn test_style_loading_from_json() {

    // Some JSON input data as a &str. Maybe this comes from the user.
    let data = r#"
        {
            "scrolltext": {
                "background_start_color": [98, 98, 98],
                "background_end_color": [112, 112, 112],
                "background_alfa": 230,

                "text_color": [255,255,255],
                "text_alfa": 255,
                "text_border_color": [0,0,0],
                "text_border_alfa": 255,
                "text_border": true,
                "scrolltext_speed": 100.0,

                "padding_left": 40.0,
                "padding_right": 40.0,
                "padding_bottom": 40.0,

                "text_size": 46.0,
                "box_height": 60.0,

                "clock_background_color": [109, 72, 92],
                "clock_background_alfa": 255,
                "clock_color": [255,255,255],
                "clock_alfa": 255,
                "clock_width": 180.0,

                "stroke_on_top_color": [199, 199, 199],
                "stroke_on_top_alfa": 255,
                "stroke_on_top_width": 1.5
            }
        }"#;

    // Parse the string of data into a Person object. This is exactly the
    // same function as the one that produced serde_json::Value above, but
    // now we are asking it for a Person as output.
    let styles_re: serde_json::Result<Styles> = serde_json::from_str(data);
    if let Err(e) = styles_re {
        println!("Error: {:?}", e)
    } else {
        println!("this comes out: {:?}", styles_re.unwrap());
    }

}