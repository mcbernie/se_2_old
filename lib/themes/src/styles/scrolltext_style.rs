
use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize, Debug)]
#[serde(default)]
pub struct ScrolltextStyle {
    
    pub background_start_color: [u8; 3],
    pub background_end_color: [u8; 3],
    pub background_alfa: u8,

    pub text_color: [u8; 3],
    pub text_alfa: u8,
    pub text_border_color: [u8; 3],
    pub text_border_alfa: u8,
    pub text_border: bool,
    pub scrolltext_speed: f32,
    
    pub padding_left: f32,
    pub padding_right: f32,
    pub padding_bottom: f32,

    pub text_size: f32,
    pub box_height: f32,

    pub clock_background_color: [u8; 3],
    pub clock_background_alfa: u8,
    pub clock_color: [u8; 3],
    pub clock_alfa: u8,
    pub clock_width: f32,

    pub stroke_on_top_color: [u8; 3],
    pub stroke_on_top_alfa: u8,
    pub stroke_on_top_width: f32,

}

impl Default for ScrolltextStyle {
    fn default() -> ScrolltextStyle { 
        ScrolltextStyle {
            background_start_color: [0x62, 0x62, 0x62],
            background_end_color: [0x70, 0x70, 0x70],
            background_alfa: 230,

            text_color: [255,255,255],
            text_alfa: 255,
            text_border_color: [0,0,0],
            text_border_alfa: 255,
            text_border: true,
            scrolltext_speed: 100.0,

            padding_left: 40.0,
            padding_right: 40.0,
            padding_bottom: 40.0,

            text_size: 46.0,
            box_height: 60.0,

            clock_background_color: [0x6d, 0x48, 0x5c],
            clock_background_alfa: 255,
            clock_color: [255,255,255],
            clock_alfa: 255,
            clock_width: 180.0,

            stroke_on_top_color: [0xc7, 0xc7, 0xc7],
            stroke_on_top_alfa: 255,
            stroke_on_top_width: 1.5,
        }

     }
} 