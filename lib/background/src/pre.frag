#version 150 core

in vec2 v_texcoord;
out vec4 Target0;

uniform sampler2D from;
uniform sampler2D to;

uniform float alfa;

vec4 getFromColor(vec2 uv) {

    return texture(from, uv * vec2(1.0));
}

vec4 getToColor(vec2 uv) {
    return texture(to, uv * vec2(1.0));
}


void main() {	
	Target0 = mix(getFromColor(v_texcoord), getToColor(v_texcoord), alfa);
}