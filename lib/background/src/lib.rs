
extern crate gl;
extern crate gl_render;
extern crate resources;

use std::mem;
use std::ptr;
use std::ffi::{CString};
use std::rc::Rc;

use gl_render::{Shader, Program, Texture, activate_texture};
use resources::{ManagedTexture, TextureManager};


#[rustfmt::skip]
static VERTICES: [f32; 20] = [
     1.0,  1.0, 0.0, 1.0, 0.0,
    -1.0,  1.0, 0.0, 0.0, 0.0,
    -1.0, -1.0, 0.0, 0.0, 1.0,
     1.0, -1.0, 0.0, 1.0, 1.0,
];

static SPEED:f32 = 0.38;
const MAX_MAPPING:f32 = 0.8;
const MIN_MAPPING:f32 = 0.2;

static INDICES: [u16; 6] = [0, 1, 2, 0, 2, 3];

#[rustfmt::skip]
static IDENTITY: [f32; 16] = [
    1.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 1.0, 0.0,
    0.0, 0.0, 0.0, 1.0,
];

enum MovementDirection {
    Forward,
    Backward,
}

pub struct Background {
    gl: gl::Gl,
    program: Program,
    attr_position: gl::types::GLint,
    attr_texture: gl::types::GLint,
    vao: gl::types::GLuint,
    vertex_buffer: gl::types::GLuint,
    vbo_indices: gl::types::GLuint,
    tex_1: Rc<ManagedTexture>,
    tex_2: Rc<ManagedTexture>,
    alfa: f32,
    direction: MovementDirection,
}

impl Background {

    pub fn new(gl: &gl::Gl) -> Background {
        
        let (vao, vertex_buffer, vbo_indices) = unsafe {
            let mut vao = mem::uninitialized();
            if gl.BindVertexArray.is_loaded() {
                gl.GenVertexArrays(1, &mut vao);
                gl.BindVertexArray(vao);
            }

            let mut vertex_buffer = mem::uninitialized();
            gl.GenBuffers(1, &mut vertex_buffer);
            gl.BindBuffer(gl::ARRAY_BUFFER, vertex_buffer);
            gl.BufferData(
                gl::ARRAY_BUFFER,
                (VERTICES.len() * mem::size_of::<f32>()) as gl::types::GLsizeiptr,
                VERTICES.as_ptr() as *const _,
                gl::STATIC_DRAW,
            );

            let mut vbo_indices = mem::uninitialized();
            gl.GenBuffers(1, &mut vbo_indices);
            gl.BindBuffer(gl::ELEMENT_ARRAY_BUFFER, vbo_indices);
            gl.BufferData(
                gl::ELEMENT_ARRAY_BUFFER,
                (INDICES.len() * mem::size_of::<u16>()) as gl::types::GLsizeiptr,
                INDICES.as_ptr() as *const _,
                gl::STATIC_DRAW,
            );

            if gl.BindVertexArray.is_loaded() {
                gl.BindBuffer(gl::ELEMENT_ARRAY_BUFFER, vbo_indices);
                gl.BindBuffer(gl::ARRAY_BUFFER, vertex_buffer);
                gl.BindVertexArray(0);
            }

            gl.BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
            gl.BindBuffer(gl::ARRAY_BUFFER, 0);

            (
                vao,
                vertex_buffer,
                vbo_indices,
            )


        };


        let (program,attr_position,attr_texture) = load_shader(&gl);
        let tex_1 = TextureManager::get_global_manager(|tm| tm.add(&gl, "resources/bgrnd_01.jpg", "background_1"));
        let tex_2 = TextureManager::get_global_manager(|tm| tm.add(&gl, "resources/bgrnd_02.jpg", "background_2"));

        //let tex_1 = Texture::from_image(gl, "resources/bgrnd_01.jpg".to_string()).unwrap();
        //let tex_2 = Texture::from_image(gl, "resources/bgrnd_02.jpg".to_string()).unwrap();

        Background {
            gl: gl.clone(),
            // gl stuff
            program,
            attr_position,
            attr_texture,
            vao,
            vertex_buffer,
            vbo_indices,
            tex_1,
            tex_2,
            alfa: 0.0,
            direction: MovementDirection::Forward,
        }
    }

    pub fn render(&mut self, dt: f32) {//, width: u32, height: u32) {


        match self.direction {
            MovementDirection::Forward => {
                self.alfa += SPEED * dt;
            },
            MovementDirection::Backward => {
                self.alfa -= SPEED * dt
            },
        };

        if self.alfa > 1.0 {
            self.alfa = 1.0;
            self.direction = MovementDirection::Backward;
        }
        if self.alfa < 0.0 {
            self.alfa = 0.0;
            self.direction = MovementDirection::Forward;
        }

        // Map a value
        let alfa = MAX_MAPPING + (MIN_MAPPING - MAX_MAPPING) * ((self.alfa - 0.0) / (1.0 - 0.0));

        self.program.set_used();

        unsafe {

            if self.gl.BindVertexArray.is_loaded() {
                self.gl.BindVertexArray(self.vao);
            }

            {
                self.gl
                    .BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.vbo_indices);
                self.gl
                    .BindBuffer(gl::ARRAY_BUFFER, self.vertex_buffer);

                // Load the vertex position
                self.gl.VertexAttribPointer(
                    self.attr_position as gl::types::GLuint,
                    3,
                    gl::FLOAT,
                    gl::FALSE,
                    (5 * mem::size_of::<f32>()) as gl::types::GLsizei,
                    ptr::null(),
                );

                // Load the texture coordinate
                self.gl.VertexAttribPointer(
                    self.attr_texture as gl::types::GLuint,
                    2,
                    gl::FLOAT,
                    gl::FALSE,
                    (5 * mem::size_of::<f32>()) as gl::types::GLsizei,
                    (3 * mem::size_of::<f32>()) as *const () as *const _,
                );

                self.gl.EnableVertexAttribArray(self.attr_position as _);
                
                self.gl.EnableVertexAttribArray(self.attr_texture as _);
            }

            activate_texture(&self.gl, self.tex_1.id(), 0);
            activate_texture(&self.gl, self.tex_2.id(), 1);
            
            let location = self
                .gl
                .GetUniformLocation(self.program.id(), b"from\0".as_ptr() as *const _);
            self.gl.Uniform1i(location, 0);

            let location = self
                .gl
                .GetUniformLocation(self.program.id(), b"to\0".as_ptr() as *const _);
            self.gl.Uniform1i(location, 1);

            let location = self
                .gl
                .GetUniformLocation(self.program.id(), b"alfa\0".as_ptr() as *const _);
            self.gl
                .Uniform1f(location, alfa);

            let location = self
                .gl
                .GetUniformLocation(self.program.id(), b"u_transformation\0".as_ptr() as *const _);
            self.gl
                .UniformMatrix4fv(location, 1, gl::FALSE, IDENTITY.as_ptr() as *const _);

            self.gl
                .DrawElements(gl::TRIANGLES, 6, gl::UNSIGNED_SHORT, ptr::null());

            self.gl.BindTexture(gl::TEXTURE_2D, 0);
            
            self.gl.UseProgram(0);

            if self.gl.BindVertexArray.is_loaded() {
                self.gl.BindVertexArray(0);
            }

            {
                self.gl.BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
                self.gl.BindBuffer(gl::ARRAY_BUFFER, 0);
            }
        }
    }

    

}


pub fn load_shader(gl: &gl::Gl) -> (Program, i32, i32) {
    
    let (program, attr_position, attr_texture) = unsafe {
        
        let vert_shader = Shader::from_vert_source(
            &gl, 
            &CString::new(include_str!("pre.vert")).unwrap(),
        ).unwrap(); 

        let frag_shader = Shader::from_frag_source(
            &gl, 
            &CString::new(include_str!("pre.frag")).unwrap(),
        ).unwrap();
        
        let program = 
            Program::from_shaders(&gl, &[vert_shader, frag_shader]).unwrap();

        let attr_position = gl.GetAttribLocation(program.id(), b"a_position\0".as_ptr() as *const _);
        let attr_texture = gl.GetAttribLocation(program.id(), b"a_texcoord\0".as_ptr() as *const _);

        (
            program,
            attr_position,
            attr_texture,
        )

    };

    (program, attr_position, attr_texture)

}