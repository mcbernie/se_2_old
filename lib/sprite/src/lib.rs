
extern crate gl;
extern crate gl_render;
extern crate cgmath;
extern crate glm;
extern crate num;

use std::mem;
use std::ptr;
use std::ffi::{CString};

use gl_render::{Shader, Program, Texture};
use glm::*;
use glm::ext::*;


#[rustfmt::skip]
static VERTICES: [f32; 24] = [
    // Pos    // Tex
    0.0, 1.0, 0.0, 1.0,
    1.0, 0.0, 1.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 

    0.0, 1.0, 0.0, 1.0,
    1.0, 1.0, 1.0, 1.0,
    1.0, 0.0, 1.0, 0.0
];

pub struct Sprite {
    gl: gl::Gl,
    program: Program,
    vao: gl::types::GLuint,
    vertex_buffer: gl::types::GLuint,
    //attr_model: gl::types::GLint,
    image: Texture,
    ortho:[[f32; 4]; 4],
    color: [f32; 3],
    pub pos:glm::Vector2<f32>,
    pub size:glm::Vector2<f32>,
}

impl Sprite {

    pub fn new_from_texture(gl: &gl::Gl, display_width: f32, display_height: f32, texture: Texture) -> Sprite {
        let (vao, vertex_buffer) = unsafe {

            // Step 1 define VertayArrayObject
            let mut vao = mem::uninitialized();
            if gl.BindVertexArray.is_loaded() {
                gl.GenVertexArrays(1, &mut vao); // generate
                gl.BindVertexArray(vao); // bind
            }

            // Step 2 define VertexBuffer
            let mut vertex_buffer = mem::uninitialized();
            gl.GenBuffers(1, &mut vertex_buffer); // generate
            gl.BindBuffer(gl::ARRAY_BUFFER, vertex_buffer); // bind

            // Fill buffer with Data of vertices
            gl.BufferData(
                gl::ARRAY_BUFFER,
                (VERTICES.len() * mem::size_of::<f32>()) as gl::types::GLsizeiptr,
                VERTICES.as_ptr() as *const _,
                gl::STATIC_DRAW,
            );

            gl.EnableVertexAttribArray(0);

            gl.VertexAttribPointer(
                0 as gl::types::GLuint,
                4,
                gl::FLOAT,
                gl::FALSE,
                (4 * mem::size_of::<f32>()) as gl::types::GLsizei,
                ptr::null(),
            );

            gl.BindBuffer(gl::ARRAY_BUFFER, 0);
            gl.BindVertexArray(0);

            (
                vao,
                vertex_buffer,
            )


        };

        let image = texture;

        let program = load_shader(&gl);

        let pos = vec2(0.0, 0.0);
        let size = vec2(0.0, 0.0);
        let ortho = Self::set_ortho(display_width, display_height);

        Sprite {
            gl: gl.clone(),
            program,
            vao,
            vertex_buffer,
            image,
            pos,
            size,
            ortho,
            color: [0.0,0.0,0.0],
        }
    }

    pub fn new(gl: &gl::Gl, display_width: f32, display_height: f32, sprite_path: String) -> Sprite {
        let image = Texture::from_image(gl, sprite_path).unwrap();

        Self::new_from_texture(gl, display_width, display_height, image)
        
    }

    fn set_ortho(width: f32, height: f32) -> [[f32; 4]; 4] {
        let ortho:[[f32; 4]; 4] = cgmath::ortho(0.0, width, height, 0.0, -1.0, 1.0).into();

        ortho
    }

    fn set_model(position: glm::Vector2<f32>, size:glm::Vector2<f32>,rotation: f32) -> glm::Matrix4<f32> {
        
        let mut m = translate(&num::one(), vec3(position.x,position.y,0.0));
        m = translate(&m, vec3(0.5 * size.x, 0.5 * size.y, 0.0)); 
        m = rotate(&m, rotation, vec3(0.0, 0.0, 1.0)); 
        m = translate(&m, vec3(-0.5 * size.x, -0.5 * size.y, 0.0));
        scale(&m, vec3(size.x, size.y, 1.0))
        
    }
    
    pub fn resize(&mut self, width: f32, height: f32) {
        self.ortho = Self::set_ortho(width, height);
    }

    pub fn set_position(&mut self, x: f32, y: f32) {
        self.pos.x = x;
        self.pos.y = y;
    }

    pub fn set_size(&mut self, width: f32, height: f32) {
        self.size.x = width;
        self.size.y = height;
    }

    pub fn set_color(&mut self, r: f32, g: f32, b: f32) {
        self.color = [r, g, b];
    }

    pub fn render(&mut self, dt: f32) {//, width: u32, height: u32) {


        let model = Self::set_model(self.pos, self.size, 0.0);

        self.program.set_used();

        unsafe {

            if self.gl.BindVertexArray.is_loaded() {
                self.gl.BindVertexArray(self.vao);
            }


            // Projections
            let ptr: *const f32 = std::mem::transmute(&self.ortho);
            let location = self
                .gl
                .GetUniformLocation(self.program.id(), b"projection\0".as_ptr() as *const _);
            self.gl
                .UniformMatrix4fv(location, 1, gl::FALSE, ptr);



            // Model
            let ptr: *const f32 = std::mem::transmute(&model);
            let location = self
                .gl
                .GetUniformLocation(self.program.id(), b"model\0".as_ptr() as *const _);
            self.gl
                .UniformMatrix4fv(location, 1, gl::FALSE, ptr);

            // color
            let location = self
                .gl
                .GetUniformLocation(self.program.id(), b"spriteColor\0".as_ptr() as *const _);
            self.gl
                .Uniform3fv(location, 1, self.color.as_ptr() as *const _);

            //sprite
            self.image.bind(0);
            let location = self
                .gl
                .GetUniformLocation(self.program.id(), b"image\0".as_ptr() as *const _);
            self.gl.Uniform1i(location, 0);

            let location = self
                .gl
                .GetUniformLocation(self.program.id(), b"dt\0".as_ptr() as *const _);
            self.gl.Uniform1f(location, dt);

            self.gl.DrawArrays(gl::TRIANGLES, 0, 6);

            
            self.gl.UseProgram(0);

            if self.gl.BindVertexArray.is_loaded() {
                self.gl.BindVertexArray(0);
            }
        }
    }

}



impl Drop for Sprite {
    fn drop(&mut self) {
        unsafe {
            self.gl.DeleteVertexArrays(1,&self.vao);
            self.gl.DeleteBuffers(1,&self.vertex_buffer);
        }
    }
}

pub fn load_shader(gl: &gl::Gl) -> Program {//, i32) {

    let vert_shader = Shader::from_vert_source(
        &gl, 
        &CString::new(include_str!("sprite.vert")).unwrap(),
    ).unwrap(); 

    let frag_shader = Shader::from_frag_source(
        &gl, 
        &CString::new(include_str!("sprite.frag")).unwrap(),
    ).unwrap();
    
    Program::from_shaders(&gl, &[vert_shader, frag_shader]).unwrap()
}