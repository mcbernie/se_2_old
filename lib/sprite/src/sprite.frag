#version 150 core

in vec2 tex_coords;
out vec4 Target0;

uniform sampler2D background;
uniform sampler2D text;

uniform vec3 spriteColor;
uniform float dt;


void main() {
    vec4 background = texture(background, tex_coords);
    //vec4 foreground = texture(text, vec2(tex_coords.x + (dt * 0.2), tex_coords.y));
    //Target0 =  vec4(spriteColor, 1.0) * background; /// foreground;

    Target0 =  background; /// foreground;
    //Target0 = vec4(1.0,0.4,0.2,1.0);
}