#[macro_use]
extern crate gstreamer as gst;
use gst::prelude::*;

extern crate gstreamer_app as gst_app;
extern crate gstreamer_gl as gst_gl;
use gst_gl::prelude::*;
extern crate gstreamer_video as gst_video;

extern crate glib;

extern crate byte_slice_cast;
use byte_slice_cast::*;

use std::error::Error as StdError;
use std::ffi::CStr;
use std::mem;
use std::ptr;
use std::sync::{mpsc, Arc, Mutex};
use std::thread;

extern crate failure;
use failure::Error;

#[macro_use]
extern crate failure_derive;

#[derive(Debug, Fail)]
#[fail(display = "Missing element {}", _0)]
struct MissingElement(&'static str);

#[derive(Debug, Fail)]
#[fail(
    display = "Received error from {}: {} (debug: {:?})",
    src, error, debug
)]
struct ErrorMessage {
    src: String,
    error: String,
    debug: Option<String>,
    #[cause]
    cause: glib::Error,
}


extern crate glutin;
//use glutin::ContextTrait;

/**
 * Video
 * Structure for rendering Video content on opengl texture
 */
pub struct Video {
    pipeline: gst::Pipeline,
    appsink: gst_app::AppSink,
    glupload: gst::Element,
    bus: gst::Bus,
    curr_frame: Option<Arc<gst_video::VideoFrame<gst_video::video_frame::Readable>>>,

    handler: thread::JoinHandle<()>, 
    receiver: mpsc::Receiver<gst::Sample>,
}

fn map_gl_api(api: glutin::Api) -> gst_gl::GLAPI {
    match api {
        glutin::Api::OpenGl => gst_gl::GLAPI::OPENGL3,
        glutin::Api::OpenGlEs => gst_gl::GLAPI::GLES2,
        _ => gst_gl::GLAPI::NONE,
    }
}

impl Video {
    /**
     * create a new Video structure, need glutin window context
     */
    pub fn new(path: String, glwindow: &glutin::ContextWrapper<glutin::PossiblyCurrent, glutin::Window>) -> Video {
        use glutin::os::unix::RawHandle; // should only work on linux systems
        use glutin::os::unix::WindowExt;
        use glutin::os::ContextTraitExt;

        gst::init().unwrap();

        let (pipeline, appsink, glupload) = Video::create_pipeline(path).unwrap();
        
        let bus = pipeline
            .get_bus()
            .expect("Pipeline without bus. Shouldn't happen!");

        let window = glwindow.window();
        let context = glwindow.context();

        let shared_context: gst_gl::GLContext;

        let api = map_gl_api( glwindow.get_api() );

        let (gl_context, gl_display, platform): (usize, gst_gl::GLDisplay, gst_gl::GLPlatform) = match unsafe { context.raw_handle() } {
        
            #[cfg(any(feature = "gl-egl", feature = "gl-wayland"))]
            RawHandle::Egl(egl_context) => {
                #[cfg(feature = "gl-egl")]
                let gl_display =
                    if let Some(display) = unsafe { window.get_egl_display() } {
                        unsafe { gst_gl::GLDisplayEGL::new_with_egl_display(display as usize) }
                            .unwrap()
                    } else {
                        panic!("EGL context without EGL display");
                    };

                #[cfg(not(feature = "gl-egl"))]
                let gl_display = if let Some(display) = window.get_wayland_display() {
                    unsafe { gst_gl::GLDisplayWayland::new_with_display(display as usize) }
                        .unwrap()
                } else {
                    panic!("Wayland window without Wayland display");
                };

                (
                    egl_context as usize,
                    gl_display.upcast::<gst_gl::GLDisplay>(),
                    gst_gl::GLPlatform::EGL,
                )
            }
            #[cfg(feature = "gl-x11")]
            RawHandle::Glx(glx_context) => {
                let gl_display = if let Some(display) = window.get_xlib_display() {
                    unsafe { gst_gl::GLDisplayX11::new_with_display(display as usize) }.unwrap()
                } else {
                    panic!("X11 window without X Display");
                };

                (
                    glx_context as usize,
                    gl_display.upcast::<gst_gl::GLDisplay>(),
                    gst_gl::GLPlatform::GLX,
                )
            }
            handler => panic!("Unsupported platform: {:?}.", handler),

        };

        shared_context =
            unsafe { gst_gl::GLContext::new_wrapped(&gl_display, gl_context, platform, api) }
                .unwrap();

        shared_context
            .activate(true)
            .expect("Couldn't activate wrapped GL context");

        shared_context.fill_info().unwrap();

        let gl_context = shared_context.clone();
    
        #[allow(clippy::single_match)]
        bus.set_sync_handler(move |_, msg| {
            use gst::MessageView;

            match msg.view() {
                MessageView::NeedContext(ctxt) => {
                    let context_type = ctxt.get_context_type();
                    if context_type == *gst_gl::GL_DISPLAY_CONTEXT_TYPE {
                        if let Some(el) =
                            msg.get_src().map(|s| s.downcast::<gst::Element>().unwrap())
                        {
                            let context = gst::Context::new(context_type, true);
                            context.set_gl_display(&gl_display);
                            el.set_context(&context);
                        }
                    }
                    if context_type == "gst.gl.app_context" {
                        if let Some(el) =
                            msg.get_src().map(|s| s.downcast::<gst::Element>().unwrap())
                        {
                            let mut context = gst::Context::new(context_type, true);
                            {
                                let context = context.get_mut().unwrap();
                                let mut s = context.get_mut_structure();
                                s.set_value("context", gl_context.to_send_value());
                            }
                            el.set_context(&context);
                        }
                    }
                }
                _ => (),
            }
            gst::BusSyncReply::Pass
        });
    
        let (handler,receiver) = Video::setup(bus.clone(), pipeline.clone(), &appsink).unwrap();

        Video {
            pipeline,
            appsink,
            bus,
            glupload,
            curr_frame: None,

            handler,
            receiver,
        }

    }

    /// create gstreamer pipeline
    /// gets called inside new function
    fn create_pipeline(path: String) -> Result<(gst::Pipeline, gst_app::AppSink, gst::Element), Error> {
        let pipeline = gst::Pipeline::new(None);
        
        let src = gst::ElementFactory::make("filesrc", None)
            .ok_or(MissingElement("filesrc"))?;
        let qtdemuxer =
            gst::ElementFactory::make("qtdemux", None).ok_or(MissingElement("qtdemux"))?;            

        let h264parse =
            gst::ElementFactory::make("h264parse", None).ok_or(MissingElement("h264parse"))?;
        let vaapidecodebin =
            gst::ElementFactory::make("vaapidecodebin", None).ok_or(MissingElement("vaapidecodebin"))?;
        let sink =
            gst::ElementFactory::make("glsinkbin", None).ok_or(MissingElement("glsinkbin"))?;

        src.set_property("location", &path)?;

        pipeline.add_many(&[&src, &qtdemuxer, &h264parse, &vaapidecodebin, &sink])?;

        src.link(&qtdemuxer)?;

        gst::Element::link_many(&[&h264parse, &vaapidecodebin, &sink])?;

        //let pipeline_clone = pipeline.clone();
        let h264parse_clone = h264parse.clone();
        qtdemuxer.connect_pad_added(move |qtdemux, src_pad| {
            handle_demux_pad_added(qtdemux, src_pad, &h264parse_clone)
        });

        qtdemuxer
            .sync_state_with_parent()
            .expect("Failed to build remux pipeline");

        let appsink = gst::ElementFactory::make("appsink", None)
            .ok_or(MissingElement("appsink"))?
            .dynamic_cast::<gst_app::AppSink>()
            .expect("Sink element is expected to be an appsink!");

        sink.set_property("sink", &appsink)?;

        appsink.set_property("enable-last-sample", &false.to_value())?;
        appsink.set_property("emit-signals", &false.to_value())?;
        appsink.set_property("max-buffers", &1u32.to_value())?;

        let caps = gst::Caps::builder("video/x-raw")
            .features(&[&gst_gl::CAPS_FEATURE_MEMORY_GL_MEMORY])
            .field("format", &gst_video::VideoFormat::Rgba.to_string())
            .field("texture-target", &"2D")
            .build();
        appsink.set_caps(Some(&caps));

        // get the glupload element to extract later the used context in it
        let mut iter = sink.dynamic_cast::<gst::Bin>().unwrap().iterate_elements();
        let glupload = loop {
            match iter.next() {
                Ok(Some(element)) => {
                    if "glupload" == element.get_factory().unwrap().get_name() {
                        break Some(element);
                    }
                }
                Err(gst::IteratorError::Resync) => iter.resync(),
                _ => break None,
            }
        };

       
        Ok((pipeline, appsink, glupload.unwrap()))
    }

    
    /// setup - create a thread and mpsc receive channel for frame and video processing
    fn setup(bus: gst::Bus, pipeline: gst::Pipeline, appsink: &gst_app::AppSink) -> Result<
        (
            thread::JoinHandle<()>, 
            mpsc::Receiver<gst::Sample>
        ), Error> {

        let bus = bus.clone();
        let pipeline = pipeline.clone();
        let bus_handler = thread::spawn(move || {
            let ret = Video::gst_loop(bus, pipeline);
            if ret.is_err() {
                eprintln!("ERROR! {:?}", ret);
            }
        });
        let (sender, receiver) = mpsc::channel();
        let sender_clone = Mutex::new(sender.clone());
        
        appsink.set_callbacks(
            gst_app::AppSinkCallbacks::new()
                .new_sample(move |appsink| {
                    let sample = appsink.pull_sample().ok_or(gst::FlowError::Eos)?;

                    sender_clone
                        .lock()
                        .unwrap()
                        .send(sample)
                        .map(|_| gst::FlowSuccess::Ok)
                        .map_err(|_| gst::FlowError::Error)
                })
                .build(),
        );

        //self.pipeline.set_state(gst::State::Playing)?;


        //Ok((bus_handler, receiver))
        Ok((bus_handler,receiver))
    }

    /// update a texture with current video frame 
    /// usual called in render / update function
    pub fn update(&mut self) -> Option<u32> {
         // get the last frame in channel
        while let Ok(sample) = self.receiver.try_recv() {
            let buffer = sample.get_buffer().unwrap();
            let info = sample
                .get_caps()
                .and_then(|caps| gst_video::VideoInfo::from_caps(caps))
                .unwrap();

            if let Ok(frame) = gst_video::VideoFrame::from_buffer_readable_gl(buffer.to_owned(), &info) {
                self.curr_frame = Some(Arc::new(frame));
            }
        }

        if let Some(frame) = self.curr_frame.clone() {   
            return frame.get_texture_id(0);
        }
        return None;
    }

    fn gst_loop(bus: gst::Bus, pipeline: gst::Pipeline) -> Result<(), Error> {
        use gst::MessageView;

        for msg in bus.iter_timed(gst::CLOCK_TIME_NONE) {
            match msg.view() {
                MessageView::Eos(..) => {
                    pipeline.set_state(gst::State::Null);
                    pipeline.set_state(gst::State::Playing);
                },
                MessageView::Error(err) => {
                    Err(ErrorMessage {
                        src: msg
                            .get_src()
                            .map(|s| String::from(s.get_path_string()))
                            .unwrap_or_else(|| String::from("None")),
                        error: err.get_error().description().into(),
                        debug: Some(err.get_debug().unwrap().to_string()),
                        cause: err.get_error(),
                    })?;
                }
                _ => (),
            }
        }

        Ok(())
    }


    pub fn play(&self) {
        self.pipeline.set_state(gst::State::Playing);
    }

    pub fn pause(&self) {
        self.pipeline.set_state(gst::State::Paused);
    }

    ///unload sink and pipeline
    ///should called with defer inside the main thread function
    pub fn unload(&self/*, bus: &mut VideoBus*/) {
        self.pipeline.send_event(gst::Event::new_eos().build());
        //bus.handler.join().expect("Could join bus handler thread");
        self.pipeline.set_state(gst::State::Null);
    }
}

// This is the callback function called by the demuxer, when a new stream was detected.
fn handle_demux_pad_added(
    demuxer: &gst::Element,
    demux_src_pad: &gst::Pad,
    h264decode: &gst::Element,
) {
    if demux_src_pad.get_name().as_str() != "video_0" {
        return;
    };
    
    let link_to_muxer = || -> Result<(), Error> {

        for s in h264decode.get_sink_pads() {
            println!("sink pad name: {:?}", s.get_name().as_str());
            demux_src_pad.link(&s)?;
        };

        Ok(())
    };

    if let Err(err) = link_to_muxer() {
        gst_element_error!(
            demuxer,
            gst::LibraryError::Failed,
            ("Failed to insert sink"),
            ["{}", err]
        );
    }
}