

#[allow(unused_variables)]
pub trait Item {
    fn update(&mut self, dt: f32) {
    }

    fn activate(&self, pos: u32);

    fn in_scope(&mut self) {
    }

    fn out_of_scope(&mut self) {
    }

    fn resize(&mut self, width: f32, height: f32) {
    }

    fn keypress(&mut self, key: glutin::VirtualKeyCode) {
    }

    fn begin_go_out_of_scope(&mut self) {
    }


    fn is_in_scope(&self) -> bool;

    fn id(&self) -> u32;

    fn pause_time(&self) -> f32 {
        5.0
    }

    /// flip the texture upsidedown inside the slideshow shader 
    /// 1.0 means from down to up (most pictures are flipped)
    /// -1.0 means the reverse of 1.0
    fn flip(&self) -> f32 {
        1.0
    }

}

pub struct Items{
    list: Vec<Box<Item>>,
    current: usize,
}

impl Items {
    pub fn new() -> Items {
        Items{
            list: Vec::new(),
            current: 0,
        }
    }

    pub fn from(&self) -> u32 {
        self.list[self.from_position()].id()
    }

    pub fn begin_go_out_of_scope(&mut self) {

        let f = self.from_position();
        self.list[f].begin_go_out_of_scope();
    }


    pub fn to_position(&self) -> usize {
        if self.current + 1 >= self.list.len() {
            0
        } else {
            self.current + 1
        }
    }

    pub fn from_position(&self) -> usize {
        self.current
    }

    pub fn to(&mut self) -> u32 {
        self.list[self.to_position()].id()
    }

    pub fn next(&mut self) {
        self.current += 1;
        if self.current >= self.list.len() {
            self.current = 0;
        }
        
    }

    pub fn let_to_in_scope(&mut self) {
        let p = self.to_position();
        self.list[p].in_scope();
    }

    pub fn let_to_out_scope(&mut self) {
        let p = self.to_position();
        self.list[p].out_of_scope();
    }

    pub fn let_from_in_scope(&mut self) {
        let p = self.from_position();
        self.list[p].in_scope();
    }

    pub fn let_from_out_scope(&mut self) {
        let p = self.from_position();
        self.list[p].out_of_scope();
    }

    pub fn get_pause_from_from(&self) -> f32 {
        let p = self.from_position();
         self.list[p].pause_time()
    }

    pub fn flip_from(&self) -> f32 {
        let p = self.from_position();
        self.list[p].flip()
    }

    pub fn flip_to(&self) -> f32 {
        let p = self.to_position();
         self.list[p].flip()
    }

    pub fn add(&mut self, i: Box<Item>) {
        self.list.push(i);
    }

    pub fn clear(&mut self) {
        self.list.clear();
    }

    pub fn update(&mut self, dt:f32) {
        //let d = Instant::now();
        let items_list = &mut self.list;
        
        for i in items_list {
            i.update(dt);
        }
    }

    pub fn resize(&mut self, width: f32, height: f32, dpi: f32) {
        for item in self.list.as_mut_slice() {
            item.resize(width * dpi, height * dpi);
        }
    }

    pub fn keypress(&mut self, key: glutin::VirtualKeyCode) {
        for item in self.list.as_mut_slice() {
            item.keypress(key)
        }
    }
}