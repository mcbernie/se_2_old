
#[macro_use]
extern crate log;

extern crate gl;
extern crate gl_render;
extern crate glutin;
extern crate glob;
extern crate num;
extern crate glm;
extern crate sprite;
extern crate lyon_geom;
extern crate background;
extern crate nanovg;
extern crate client;

#[cfg(target_os = "linux")]
extern crate video_gstreamer;


#[cfg(target_os = "macos")]
extern crate video_ffmpeg;

use std::mem;
use std::ptr;
use std::ffi::{CString};
use std::time::Instant;

use gl_render::{Texture, activate_texture, Shader, Program, RawTexture};


pub mod things;

mod items;
pub use items::{Items, Item};

use std::fs::{File};
use std::io::Read;
use std::path::Path;

use std::thread;
use std::sync::mpsc;
use std::sync::mpsc::{Sender, Receiver};
//use std::marker::Send;

use glob::glob;

use rand::thread_rng;

use rand::seq::SliceRandom;

/// Hier mach ich was nices mit slideshows...
/// hier kommt auch die slideshow statemachine
/// 

enum RenderState {
    Pause,
    FromPauseToTransition,
    Transition,
    FromTransitionToPause,
}

#[rustfmt::skip]
static VERTICES: [f32; 20] = [
     1.0,  1.0, 0.0, 1.0, 0.0,
    -1.0,  1.0, 0.0, 0.0, 0.0,
    -1.0, -1.0, 0.0, 0.0, 1.0,
     1.0, -1.0, 0.0, 1.0, 1.0,
];


static INDICES: [u16; 6] = [0, 1, 2, 0, 2, 3];

#[rustfmt::skip]
static IDENTITY: [f32; 16] = [
    1.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 1.0, 0.0,
    0.0, 0.0, 0.0, 1.0,
];


pub struct Slideshow {
    gl: gl::Gl,
    time_start: Instant,
    progress: f32,
    state: RenderState,
    items: Items,
    program: Program,
    attr_position: gl::types::GLint,
    attr_texture: gl::types::GLint,
    vao: gl::types::GLuint,
    vertex_buffer: gl::types::GLuint,
    vbo_indices: gl::types::GLuint,

    all_shaders: Vec<String>,
    current_shader: usize,

    items_tx: Sender<RawTexture>,
    items_rx: Receiver<RawTexture>,

    pause_time: f32,
}


impl Slideshow {

    pub fn new(gl: &gl::Gl, _window_context: &glutin::ContextWrapper<glutin::PossiblyCurrent, glutin::Window>) -> Slideshow {

        info!("generate VertexBuffer and VertexBufferObjects");
        let (vao, vertex_buffer, vbo_indices) = unsafe {
            let mut vao = mem::uninitialized();
            if gl.BindVertexArray.is_loaded() {
                gl.GenVertexArrays(1, &mut vao);
                gl.BindVertexArray(vao);
            }

            let mut vertex_buffer = mem::uninitialized();
            gl.GenBuffers(1, &mut vertex_buffer);
            gl.BindBuffer(gl::ARRAY_BUFFER, vertex_buffer);
            gl.BufferData(
                gl::ARRAY_BUFFER,
                (VERTICES.len() * mem::size_of::<f32>()) as gl::types::GLsizeiptr,
                VERTICES.as_ptr() as *const _,
                gl::STATIC_DRAW,
            );

            let mut vbo_indices = mem::uninitialized();
            gl.GenBuffers(1, &mut vbo_indices);
            gl.BindBuffer(gl::ELEMENT_ARRAY_BUFFER, vbo_indices);
            gl.BufferData(
                gl::ELEMENT_ARRAY_BUFFER,
                (INDICES.len() * mem::size_of::<u16>()) as gl::types::GLsizeiptr,
                INDICES.as_ptr() as *const _,
                gl::STATIC_DRAW,
            );

            if gl.BindVertexArray.is_loaded() {
                gl.BindBuffer(gl::ELEMENT_ARRAY_BUFFER, vbo_indices);
                gl.BindBuffer(gl::ARRAY_BUFFER, vertex_buffer);
                gl.BindVertexArray(0);
            }

            gl.BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
            gl.BindBuffer(gl::ARRAY_BUFFER, 0);

            (
                vao,
                vertex_buffer,
                vbo_indices,
            )


        };

        let mut items = Items::new();
        let from = 
            Texture::from_image(&gl, "./resources/images/default.jpg".to_string()).unwrap();
        items.add(Box::new(from));

        let mut shaders: Vec<String> = Vec::new();
    
        info!("load shader from resources");
        for entry in glob("./resources/shader/*.glsl").unwrap() {
            shaders.push(entry.unwrap().to_str().unwrap().to_string());
        }
        shaders.shuffle(&mut thread_rng());
        
        let (program,attr_position,attr_texture) = load_shader(&gl, "./resources/shader/bounce.glsl".to_string());


        let (items_tx, items_rx) = mpsc::channel();

        Slideshow {
            gl: gl.clone(),
            // gl stuff
            program,
            attr_position,
            attr_texture,
            vao,
            vertex_buffer,
            vbo_indices,

            time_start: Instant::now(),
            progress: 0.0,
            state: RenderState::Pause,
            items,
            current_shader: 0,
            all_shaders: shaders,

            items_tx,
            items_rx,

            pause_time: 5.0,
        }
    }

    pub fn load_default_items(&mut self, _window_context: &glutin::ContextWrapper<glutin::PossiblyCurrent, glutin::Window>) {
        info!("load default images and statistics");
        let stat_test = 
            things::Statistic::new(&self.gl, 1024.0, 768.0);

        /*#[cfg(target_os = "linux")]
        let video1 =
            Video::new(&self.gl, window_context, "/home/setest/tr5_event_bally.mp4".to_string());   

        
        #[cfg(target_os = "macos")]
        let video1 =
            Video::new(&self.gl, window_context, "/Users/nico/.go/src/github.com/mcbernie/myopengl/assets/video/tr5_event_bally.mp4".to_string()); 

        self.items.add(Box::new(video1));*/
        self.items.add(Box::new(stat_test));
        
    }

    pub fn replace_items(&mut self, list: &client::List, window_context: &glutin::ContextWrapper<glutin::PossiblyCurrent, glutin::Window>) {
        let stat_test = 
            things::Statistic::new(&self.gl, 1024.0, 768.0);

        self.items.clear();
        self.items.add(Box::new(stat_test));

        use client::MediaType;
        for item in list.list.iter() {
            if item.media_type == MediaType::Video {
                debug!("load video...");
                match things::VideoMedia::load(item.clone(), &self.gl, window_context) {
                    Ok(media) => self.items.add(Box::new(media)),
                    Err(e) => error!("Could not load Video:{:?}", e )
                };
            } else {
                match things::Media::load(item.clone(), &self.gl) {
                    Ok(media) => self.items.add(Box::new(media)),
                    Err(e) => error!("Could not load Image:{:?}", e )
                };
            }
            
            
        };
    }

    pub fn load_statistics(&mut self) {
        let stat_test = 
            things::Statistic::new(&self.gl, 1024.0, 768.0);
        self.items.add(Box::new(stat_test));
    }

    fn change_shader(&mut self) {
        debug!("change shader");
        let shader_file = &self.all_shaders[self.current_shader];
        let (program,attr_position,attr_texture) = load_shader(&self.gl, shader_file.to_string());

        self.program = program;
        self.attr_position = attr_position;
        self.attr_texture = attr_texture;

        self.current_shader += 1;
        if self.current_shader >= self.all_shaders.len() {
            self.current_shader = 0;
        }
    }


    pub fn update(&mut self, global_dt:f32) {
        let delta = self.time_start.elapsed();
        let delta = delta.as_secs() as f32 + delta.subsec_nanos() as f32 / 1000_000_000.0;

        match self.state {
            RenderState::Transition => {
                if delta >= 1.0 {
                    self.state = RenderState::FromTransitionToPause;
                } else {
                    self.progress = delta / 1.0;
                }
            }

            RenderState::FromTransitionToPause => {
                self.time_start = Instant::now();
                self.progress = 0.0;
                // hier muss das from stoppen, to abspielen
                self.items.let_from_out_scope();
                self.items.next();
                self.change_shader();

                self.pause_time = self.items.get_pause_from_from();
                

                self.state = RenderState::Pause;
            },
            RenderState::Pause => {
                if delta >= self.pause_time {
                    self.state = RenderState::FromPauseToTransition;
                } else {

                    let received_texture = self.items_rx.try_recv();
                    if received_texture.is_ok() {
                        debug!("received new texture ->");
                        let mut raw_texture = received_texture.unwrap();

                        self.items.add(Box::new(
                            raw_texture.to_texture(&self.gl)
                        ));
                    }
                }
            },
            RenderState::FromPauseToTransition => {
                self.time_start = Instant::now();

                self.items.begin_go_out_of_scope();

                self.items.let_to_in_scope();
                self.state = RenderState::Transition;
            },
        }

        self.items.update(global_dt);

    }

    pub fn render(&mut self) {
        self.program.set_used();

        unsafe {

            if self.gl.BindVertexArray.is_loaded() {
                self.gl.BindVertexArray(self.vao);
            }

            {
                self.gl
                    .BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.vbo_indices);
                self.gl
                    .BindBuffer(gl::ARRAY_BUFFER, self.vertex_buffer);

                // Load the vertex position
                self.gl.VertexAttribPointer(
                    self.attr_position as gl::types::GLuint,
                    3,
                    gl::FLOAT,
                    gl::FALSE,
                    (5 * mem::size_of::<f32>()) as gl::types::GLsizei,
                    ptr::null(),
                );

                // Load the texture coordinate
                self.gl.VertexAttribPointer(
                    self.attr_texture as gl::types::GLuint,
                    2,
                    gl::FLOAT,
                    gl::FALSE,
                    (5 * mem::size_of::<f32>()) as gl::types::GLsizei,
                    (3 * mem::size_of::<f32>()) as *const () as *const _,
                );

                self.gl.EnableVertexAttribArray(self.attr_position as _);
                
                self.gl.EnableVertexAttribArray(self.attr_texture as _);
            }


            activate_texture(&self.gl, self.items.from(), 0);
            activate_texture(&self.gl, self.items.to(), 1);
            
            let location = self
                .gl
                .GetUniformLocation(self.program.id(), b"from\0".as_ptr() as *const _);
            self.gl.Uniform1i(location, 0);

            let location = self
                .gl
                .GetUniformLocation(self.program.id(), b"to\0".as_ptr() as *const _);
            self.gl.Uniform1i(location, 1);


            let location = self
                .gl
                .GetUniformLocation(self.program.id(), b"progress\0".as_ptr() as *const _);
            self.gl.Uniform1f(location, self.progress);

            let location = self
                .gl
                .GetUniformLocation(self.program.id(), b"flip_from\0".as_ptr() as *const _);
            self.gl.Uniform1f(location, self.items.flip_from());

            let location = self
                .gl
                .GetUniformLocation(self.program.id(), b"flip_to\0".as_ptr() as *const _);
            self.gl.Uniform1f(location, self.items.flip_to());

            let location = self
                .gl
                .GetUniformLocation(self.program.id(), b"u_transformation\0".as_ptr() as *const _);
            self.gl
                .UniformMatrix4fv(location, 1, gl::FALSE, IDENTITY.as_ptr() as *const _);

            self.gl
                .DrawElements(gl::TRIANGLES, 6, gl::UNSIGNED_SHORT, ptr::null());

            self.gl.BindTexture(gl::TEXTURE_2D, 0);
            
            self.gl.UseProgram(0);

            if self.gl.BindVertexArray.is_loaded() {
                self.gl.BindVertexArray(0);
            }

            {
                self.gl.BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
                self.gl.BindBuffer(gl::ARRAY_BUFFER, 0);
            }
        }
    }

    pub fn resize(&mut self, width: f32, height: f32, dpi: f32) {
        self.items.resize(width, height, dpi);
    }

    pub fn keypress(&mut self, key: glutin::VirtualKeyCode) {
        self.items.keypress(key);
    }

    pub fn get_item_loader_fn(&mut self) -> impl Fn(String) {
        let tx = self.items_tx.clone();

        move |path: String| {

            let tx = tx.clone();

            thread::spawn(move || {
                let tex = RawTexture::from_file(path).unwrap();
                debug!("send loaded Texture->");
                tx.send(tex).unwrap();
            });


        }
        
    }

}


pub fn load_shader(gl: &gl::Gl, path: String) -> (Program, i32, i32) {
    
    let (program, attr_position, attr_texture) = unsafe {
        
        let vert_shader = Shader::from_vert_source(
            &gl, 
            &CString::new(include_str!("pre.vert")).unwrap(),
        ).unwrap(); 


        // hier combiniere ich
        let prepend = include_str!("pre.frag");

        let descent = include_str!("end.frag");

        debug!("file -> {}", path);
        let mut f = File::open(Path::new(&path)).unwrap();

        let mut buffer = String::new();

        let _s = f.read_to_string(&mut buffer);

        let mut fragment_string = String::new();

        fragment_string.push_str(prepend);
        fragment_string.push_str(&buffer);
        fragment_string.push_str(descent);

        let frag_shader = Shader::from_frag_source(
            &gl, 
            &CString::new(fragment_string).unwrap(),
        ).unwrap();
        
        let program = 
            Program::from_shaders(&gl, &[vert_shader, frag_shader]).unwrap();

        let attr_position = gl.GetAttribLocation(program.id(), b"a_position\0".as_ptr() as *const _);
        let attr_texture = gl.GetAttribLocation(program.id(), b"a_texcoord\0".as_ptr() as *const _);


        (
            program,
            attr_position,
            attr_texture,
        )

    };

    (program, attr_position, attr_texture)

}