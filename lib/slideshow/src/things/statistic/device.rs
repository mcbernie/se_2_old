

use super::consts::{*};

use glm::{Vector2};
use lyon_geom::{QuadraticBezierSegment, euclid};

use rand::{thread_rng, Rng};
use nanovg::{StrokeOptions, Color, Gradient, TextOptions, Alignment};

pub struct Device {
    position: Vector2<f32>,
    size: Vector2<f32>,

    bezier: QuadraticBezierSegment<f32>,
    bezier_pos: f32,

    own_position: i32,

    name: String,
    value: i32,
    
    pub direction: AnimationDirection,
}

impl Device {
    pub fn new(name: &'static str, value: i32, own_position: i32, display_width: f32, display_height: f32) -> Device {

        let x_b = thread_rng().gen_range(0.0, display_width);
        let y_b = thread_rng().gen_range(0.0, display_height);
        let position = glm::vec2(0.0,0.0);

        let (x, y, width, height) = Self::set_ratio_and_size(own_position, display_width, display_height);


        Device {
            position ,
            size: glm::vec2(width, height),
            bezier: QuadraticBezierSegment {
                from: euclid::Point2D::new(display_width, display_height / 2.0),
                to: euclid::Point2D::new(x, y),
                ctrl: euclid::Point2D::new(x_b, y_b)
            },
            bezier_pos: 0.0,
            own_position,
            name: name.to_string(),
            value,
            direction: AnimationDirection::Forward,
        }


    }

    pub fn set_ratio_and_size(own_position: i32, display_width: f32, display_height: f32) -> (f32, f32, f32, f32) {

        let y = {
            if own_position < DEVICES_COUNT / 2 { // left
                ratio(HEADER_HEIGHT, display_height) + ratio(PADDING_TOP, display_height) + ((own_position - 1) as f32 * (ratio(ITEM_HEIGHT, display_height) + ratio(PADDING_BETWEEN_ITEMS, display_height)))

            } else { // right
                ratio(HEADER_HEIGHT, display_height) + ratio(PADDING_TOP, display_height) + ( (own_position - (DEVICES_COUNT / 2)) as f32 * (ratio(ITEM_HEIGHT, display_height) + ratio(PADDING_BETWEEN_ITEMS, display_height)))
            }
        };

        let x = {
            if own_position < DEVICES_COUNT / 2 { // left
                ratio(PADDING_SIDE, display_width)
            } else { // right
                display_width - ( ratio(PADDING_SIDE, display_width) + ratio(ITEM_WIDTH, display_width) )
            }
        };
        (x, y, ratio(ITEM_WIDTH, display_width),ratio(ITEM_HEIGHT, display_height))
    }

    pub fn resize(&mut self, display_width: f32, display_height: f32) {
        let (x, y, width, height) = Self::set_ratio_and_size(self.own_position, display_width, display_height);
        self.bezier.to = euclid::Point2D::new(x, y);
        self.size = glm::vec2(width, height);
    }

    pub fn render(&mut self, _dt: f32, frame: &nanovg::Frame, fonts: &StatisticFonts ) {
        
        self.draw_box(&frame);
        // Set number_box size
        
        let nb_size_height = ITEM_NUMBER_BOX_HEIGHT * self.size.y;
        let item_size_height = ITEM_BOX_HEIGHT * nb_size_height;
        let item_text_space = ITEM_BOX_TEXT_SPACE * self.size.x;
        
        //self.draw_vote(&frame, fonts.icons);
        
        use super::consts::Colors;
        let number_text_options = TextOptions {
            color: Colors::get(Colors::number_color), //F47826
            size: NUMBER_FONT_SIZE * self.size.y,
            align: Alignment::new().center().middle(),
            ..Default::default()
        };

        let name_text_options = TextOptions {
            color: Colors::get(Colors::text_color),
            size: FONT_SIZE * self.size.y,
            align: Alignment::new().left().middle(),
            ..Default::default()
        };

        let name = &self.name;
        frame.text(
            fonts.sans,
            ( self.position.x + (nb_size_height / 2.0) , self.position.y + (nb_size_height / 2.0) ),
            format!("{}", self.own_position),
            number_text_options
        );
        frame.text(
            fonts.sans,
            ( self.position.x + nb_size_height + item_text_space , (self.position.y + nb_size_height) - item_size_height / 2.0 ),
            name,
            name_text_options
        );
    }

    /*fn show_value(x: f32, y: f32, sprite: &mut Sprite) -> f32 {
        sprite.set_position(x,y);
        sprite.render(0.0);
        x
    }*/

    pub fn update(&mut self, dt: f32) -> RenderChange {
        match self.direction {
            AnimationDirection::Forward => {
                if self.bezier_pos >= 1.0 {
                    return RenderChange::NothingChanged;
                }

                if self.bezier_pos <= 1.0 {
                    self.bezier_pos += SPEED * dt;  
                    if self.bezier_pos > 1.0 {
                        self.bezier_pos = 1.0;
                    }
                    let re = self.bezier.sample(self.bezier_pos);
                    self.position.x = re.x;
                    self.position.y = re.y;
                }

                RenderChange::NeedRerender
            },
            AnimationDirection::Backward => {
                if self.bezier_pos <= -1.0 {
                    return RenderChange::NothingChanged;
                }

                if self.bezier_pos >= -1.0 {
                    self.bezier_pos -= SPEED * dt;  
                    if self.bezier_pos < -1.0 {
                        self.bezier_pos = -1.0;
                    }
                    let re = self.bezier.sample(self.bezier_pos);
                    self.position.x = re.x;
                    self.position.y = re.y;
                }

                RenderChange::NeedRerender
            }
        }
        
    }

    pub fn reset_bezier(&mut self) {
        self.bezier_pos = 0.0;
    }

    fn draw_box(&mut self, frame: &nanovg::Frame) {
        let x = self.position.x;
        let y = self.position.y;

        

        let w = self.size.x;
        let h = self.size.y;

        let small_y = y + (h - ratio(ITEM_BOX_HEIGHT ,h ) );
        let small_h = ratio(ITEM_BOX_HEIGHT ,h );


        let corner_radius = CORNER_RADIUS * w;
        //let shadow_distance = CORNER_RADIUS * w;
        let border_thickness = BORDER_THICKNESS * w;
        
        use super::consts::Colors;
        
        //Draw Shadow
        frame.path(
            |path| {
                path.rounded_rect((x, small_y), (w, small_h), corner_radius);
                path.fill(
                    Gradient::Box {
                        position: (x, y + 2.0),
                        size: (w, h),
                        radius: corner_radius * 2.0,
                        feather: 10.0,
                        start_color: Color::from_rgba(0, 0, 0, 128),
                        end_color: Color::from_rgba(0, 0, 0, 0),
                    },
                    Default::default()
                );
            },
            Default::default(),
        );

        // Draw Box
        frame.path(
            |path| {
                path.rounded_rect((x, small_y ), (w, small_h), corner_radius);
                path.fill(
                    Gradient::Linear {
                        start: (0.0, small_y),
                        end: (0.0, small_y + small_h),
                        start_color: Colors::get(Colors::box_color_start),
                        end_color: Colors::get(Colors::box_color_end),
                    },
                    Default::default()
                );
            },
            Default::default(),
        );

        // border
        frame.path(
            |path| {
                path.rounded_rect((x + (border_thickness / 2.0), small_y + (border_thickness / 2.0)), (w - border_thickness, small_h - border_thickness), corner_radius - (border_thickness / 2.0));
                path.stroke(Colors::get(Colors::box_border_color), StrokeOptions {
                    width: border_thickness,
                    ..Default::default()
                });
            },
            Default::default(),
        );

        // Whitebox Box
        frame.path(
            |path| {
                path.rounded_rect((x,y), (h,h), corner_radius);
                path.fill(
                    Gradient::Linear {
                        start: (x+h, y),
                        end: (x + (h / 4.0), (y+h) - (h / 4.0) ),
                        start_color: Colors::get(Colors::number_start_color),
                        end_color: Colors::get(Colors::number_end_color),
                    },
                    Default::default()
                );
            },
            Default::default(),
        );
 

    }

    fn draw_vote(&mut self, frame: &nanovg::Frame, icon_font: nanovg::Font){
        let total = 5;

        let (positiv, active) = if self.value < 0 {
            (self.value as f32 * -1.0, IconColorState::Negativ)
        } else {
            (self.value as f32, IconColorState::Positiv)
        };

        for n in 0..(total - positiv as i32) {
            self.draw_single_vote(frame, icon_font, n, &IconColorState::Deactivated);
        }

        for n in (total - positiv as i32)..5 {
            self.draw_single_vote(frame, icon_font, n, &active);
        }

    } 

    fn draw_single_vote(&mut self, frame: &nanovg::Frame, icon_font: nanovg::Font, pos: i32, state: &IconColorState) {
        let w = self.size.x;
        let h = self.size.y;
        let border_thickness = BORDER_THICKNESS * w;
        let space = border_thickness * 1.5;
        let radius = (h / 2.0) - (space * 2.0);
        

        let x = (self.position.x + w) - (radius + space);
        let y = self.position.y + (h / 2.0);

        let x = {
            x - ((radius / 2.0) * pos as f32)
        };

        let color = {
            match state {
                IconColorState::Positiv => {
                    Color::from_rgba(0x7E, 0xD3, 0x21, 255)
                },
                IconColorState::Negativ => {
                    Color::from_rgba(0xF5, 0xA6, 0x23, 255)
                },
                _ => {
                    Color::from_rgba(0x5C, 0x5C, 0x5C, 255)
                }
            }
        };

        let icon = match self.own_position {
            x if x > DEVICES_COUNT / 2 => {
                ICON_CHEVRON_DOWN
            },
            _ => {
                ICON_CHEVRON_UP
            },
        };

        frame.path(
            |path| {
                path.circle((x,y), radius);
                path.fill(color, Default::default());
            },
            Default::default(),
        );

        frame.text(icon_font, 
            (x - ((radius / 2.0) + (radius / 4.0)), y), 
            icon, 
            TextOptions {
                color: Color::from_rgba(0, 0, 0, 255),
                size: h * 1.5,
                align: Alignment::new().left().middle(),
                ..Default::default()
            },
        );

        // button border
        frame.path(
            |path| {
                path.circle((x,y), radius - 1.0);
                path.stroke(Color::from_rgba(0, 0, 0, 200), StrokeOptions {
                    width: 2.0,
                    ..Default::default()
                });
            },
            Default::default(),
        );
        

    }
}