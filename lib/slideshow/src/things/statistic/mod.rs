// Show statistics and animate it

mod consts;
mod device;
use device::Device;
use consts::{*};

use gl_render::{activate_texture};
use gl;

use std::mem;
use std::ptr;

use background::Background;
use nanovg::{Font, Color, Frame, Path};

use crate::Item;


pub struct Statistic{
    devices: Vec<Device>,
    framebuffer_id: gl::types::GLuint,
    texture_id: gl::types::GLuint,
    gl: gl::Gl,
    in_scope_state: bool,
    display_width: f32,
    display_height: f32,
    background: Background,
    direction: AnimationDirection,
    context: nanovg::Context,
}

impl Statistic {

    pub fn new(gl: &gl::Gl, display_width: f32, display_height: f32) -> Statistic {
        
        let context = nanovg::ContextBuilder::new().stencil_strokes().build().expect("Initialization of NanoVG failed!");


        Font::from_file(&context, "DinAlternateBold", "resources/din_alternate_bold.ttf")
                    .expect("Faile to load font 'din_alternate_bold.otf'");
        Font::from_file(&context, "Entypo", "resources/entypo.ttf")
                        .expect("Failed to load font 'entypo.ttf'");
 

        let devices = vec![
            Device::new("GERAET 1f", 5, 1, display_width, display_height),
            Device::new("SUPER 41c2", 3, 2, display_width, display_height),
            Device::new("HA", 3, 3, display_width, display_height),
            Device::new("FAD 442", 2, 4,display_width, display_height),
            Device::new("ADDDFFF 45", 1, 5, display_width, display_height),
            Device::new("MEIN FA", 1, 6, display_width, display_height),
            Device::new("WASSDFfj", 0, 7, display_width, display_height),
            Device::new("GERÄTE 445", -5, 12, display_width, display_height),
            Device::new("WAS WEISS ICH", -4, 13, display_width, display_height),
        ];



        let (framebuffer_id, texture_id) = Self::generate_framebuffer(gl);

        let background = Background::new(gl);
        
        //panic!("Debug some framebuffer creation");
        Statistic {
            direction: AnimationDirection::Forward,
            framebuffer_id,
            texture_id,
            devices,
            gl: gl.clone(),
            in_scope_state: false,
            display_height,
            display_width,
            background,
            context,
        }

    }

    fn generate_framebuffer(gl: &gl::Gl) -> (gl::types::GLuint, gl::types::GLuint) {

        let (width, height) = {
            let mut current_viewport: [i32;4] = [0,0,0,0]; 
            unsafe { gl.GetIntegerv(gl::VIEWPORT, current_viewport.as_mut_ptr()) };
            (current_viewport[2], current_viewport[3])
        };

        unsafe {
            let mut framebuffer_id = mem::uninitialized();
            if gl.BindFramebuffer.is_loaded() {
                gl.GenFramebuffers(1, &mut framebuffer_id);
            }

            let texture_id = update_framebuffer_size(gl, framebuffer_id, width, height, None);

            (framebuffer_id, texture_id)
        }
        

    }

    pub fn render(&mut self, dt: f32) {
        if self.in_scope_state == true {
            
            let need_render = {
                let mut do_rerendering = false;
                for device in self.devices.iter_mut() {
                    match device.update(dt) {
                        RenderChange::NeedRerender => do_rerendering = true,
                        _ => {},
                    };
                }

                do_rerendering
            };
            
            if need_render == true {

                unsafe {
                    
                    if self.gl.BindFramebuffer.is_loaded() {
                        self.gl.BindFramebuffer(gl::FRAMEBUFFER, self.framebuffer_id);
                    }

                    self.gl.ClearColor(0.0, 0.0, 0.0, 1.0);
                    self.gl.Clear(gl::COLOR_BUFFER_BIT);
                    
                }
                self.background.render(dt);
                let context = &self.context;
                let fonts = StatisticFonts {
                    sans: Font::find(context, "DinAlternateBold").unwrap(),
                    icons: Font::find(context, "Entypo").unwrap(),
                };

                let devices_iteration = self.devices.iter_mut();

                let width = self.display_width;
                let left_padding = Window::LEFT * width;
                let top_padding  = Window::TOP * self.display_height;
                let window_height = Window::HEIGHT * self.display_height;
                let window_width = Window::WIDTH * width;
                


                context.frame((width, self.display_height), 1.0, |frame| {
                    frame.path(
                        |path| {
                            path.rect(
                                (left_padding, top_padding), 
                                (window_width, window_height));
                            path.fill(
                                Color::from_rgba(0xd8, 0xd8, 0xd8, 64),
                                Default::default()
                            );
                        },
                        Default::default(),
                    );

                    frame.path(
                        |path| {
                            path.rect(
                                (width - (left_padding + window_width), top_padding), 
                                (window_width, window_height));
                            path.fill(
                                Color::from_rgba(0xd8, 0xd8, 0xd8, 64),
                                Default::default()
                            );
                        },
                        Default::default(),
                    );

                    for device in devices_iteration {
                        device.render(dt, &frame, &fonts);                            
                    }
                });


                unsafe {
                    self.gl.BindFramebuffer(gl::FRAMEBUFFER, 0);
                }        
            }
            
        }
    }

    fn reset_bezier(&mut self) {
        for device in self.devices.iter_mut() {
            device.reset_bezier();
        }
    }
}


impl Item for Statistic {

    fn update(&mut self, dt:f32) {
        self.render(dt);
    }

    fn activate(&self, pos: u32) {
        activate_texture(&self.gl, self.texture_id, pos);
    }


    fn in_scope(&mut self) {
        self.in_scope_state = true;
        self.direction = AnimationDirection::Forward;
        for device in self.devices.iter_mut() {
            device.direction = AnimationDirection::Forward;
        }
    }

    fn out_of_scope(&mut self) {
        self.in_scope_state = false;
        debug!("reset bezier");
        self.reset_bezier();
        
    }

    fn id(&self) -> u32 {
        self.texture_id
    }

    fn is_in_scope(&self) -> bool {
        self.in_scope_state
    } 

    fn flip(&self) -> f32 {
        -1.0
    }

    fn begin_go_out_of_scope(&mut self) {
        self.direction = AnimationDirection::Backward;
        for device in self.devices.iter_mut() {
            device.direction = AnimationDirection::Backward;
        }
    }


    fn resize(&mut self, width: f32, height: f32) {
        let texture_id = update_framebuffer_size(&self.gl, self.framebuffer_id, width as i32, height as i32, Some(self.texture_id));
        self.texture_id = texture_id;

        for device in self.devices.iter_mut() {
            device.resize(width, height);
        }

        self.display_width = width;
        self.display_height = height;
    }

    fn pause_time(&self) -> f32 {
        15.0
    }
}



fn update_texture_size(gl: &gl::Gl, texture_id: gl::types::GLuint, width: i32, height: i32) {
    unsafe {
        gl.BindTexture(gl::TEXTURE_2D, texture_id);
        gl.TexImage2D(gl::TEXTURE_2D, 0, gl::RGB as i32, width , height , 0, gl::RGB, gl::UNSIGNED_BYTE, ptr::null());
    }
}

fn update_framebuffer_size(gl: &gl::Gl, framebuffer_id: gl::types::GLuint, width: i32, height: i32, old_texture_id: Option<gl::types::GLuint>) -> gl::types::GLuint {
    let texture_id = unsafe {
        if gl.BindFramebuffer.is_loaded() {
            gl.BindFramebuffer(gl::FRAMEBUFFER, framebuffer_id);
        }

        let mut texture_id = mem::uninitialized();
        gl.GenTextures(1, &mut texture_id);

        update_texture_size(&gl, texture_id, width, height);

        gl.TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::NEAREST as i32);
        gl.TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::NEAREST as i32);

        gl.FramebufferTexture2D(gl::FRAMEBUFFER, gl::COLOR_ATTACHMENT0, gl::TEXTURE_2D, texture_id, 0);

        // Set the list of draw buffers.

        let dbuffers = [gl::COLOR_ATTACHMENT0];
        gl.DrawBuffers(1, dbuffers.as_ptr() as *const _); // "1" is the size ofDrawBuffers

        gl.BindFramebuffer(gl::FRAMEBUFFER, 0);

        if let Some(old) = old_texture_id {
            info!("remove unused texture");
            gl.DeleteTextures(1, &old);
        }
        texture_id
    };

    texture_id
}
