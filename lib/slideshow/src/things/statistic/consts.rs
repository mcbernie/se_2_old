use nanovg::{Font};

// BASE OF 1024x768 pixel resolution
pub const PADDING_TOP: f32 = 10.0 / 768.0;
pub const PADDING_SIDE: f32 = 75.0 / 1280.0;
pub const PADDING_BETWEEN_ITEMS: f32= 10.0 / 768.0;
pub const HEADER_HEIGHT: f32 = 58.0 / 768.0;

//background window
pub mod Window {
    pub const LEFT:f32 = 60.0 / 1280.0;
    pub const TOP:f32 = 60.0 / 768.0;
    pub const HEIGHT: f32 = 648.0 / 768.0;
    pub const WIDTH: f32 = 570.0 / 1280.0;
}



pub mod Colors {
    use nanovg::{Color, Gradient};

    pub type CType = (u8, u8, u8, u8);
    pub type Linear = (CType, CType, (f32, f32), (f32,f32));
    /*pub struct ColorTheme(u8, u8, u8, u8);

    impl From<ColorTheme> for Color {
        fn from(c: ColorTheme) -> Self {
            //let cv = c.0;

            Color::from_rgba(c.0, c.1, c.2, c.3)
        }
    }

    impl From<(u8, u8, u8, u8)> for ColorTheme {
        fn from(c: CType) -> Self {
            //let cv = c.0;

            ColorTheme{0:c.0, 1:c.1, 2:c.2, 3:c.3}
        }
    }*/

    pub fn get(c: CType) -> Color {
        Color::from_rgba(c.0, c.1, c.2, c.3)
    }
    pub fn lin(c: Linear) -> Gradient {
        Gradient::Linear {
            start: c.2,
            end: c.3,
            start_color: get(c.0),
            end_color: get(c.1),
        }
    }


    /*
    pub static number_color: CType      = (0xf4, 0x78, 0x26, 0xff);
    pub static text_color: CType        = (0xff, 0xff, 0xff, 0xff);
    pub static box_color_start: CType   = (0xf7, 0x92, 0x1d, 0xff);
    pub static box_color_end: CType     = (0xf4, 0x78, 0x26, 0xff);
    pub static box_border_color: CType  = (0xe2, 0xe2, 0xe2, 0xff);
    pub static number_start_color: CType       = (0xc2, 0xc2, 0xc2, 0xff);
    pub static number_end_color: CType         = (0xf5, 0xf5, 0xf5, 0xff);
    */

    pub static number_color: CType      = (0x6d, 0x48, 0x5c, 0xff); //6D485C
    pub static text_color: CType        = (0xff, 0xff, 0xff, 0xff);
    pub static box_color_start: CType   = (0x85, 0x5c, 0x7c, 0xff); //855C7C
    pub static box_color_end: CType     = (0x6d, 0x48, 0x5c, 0xff); //6D485C
    pub static box_border_color: CType  = (0xe2, 0xe2, 0xe2, 0xff);
    pub static number_start_color: CType       = (0xc2, 0xc2, 0xc2, 0xff);
    pub static number_end_color: CType         = (0xf5, 0xf5, 0xf5, 0xff);
                    
}


// ITEM / BOX
pub const ITEM_WIDTH_REAL: f32 = 540.0;
pub const ITEM_HEIGHT_REAL: f32 = 44.0;
pub const ITEM_WIDTH: f32 = ITEM_WIDTH_REAL / 1280.0;
pub const ITEM_HEIGHT: f32 = ITEM_HEIGHT_REAL / 768.0;
pub const ITEM_NUMBER_BOX_HEIGHT:f32 = ITEM_HEIGHT_REAL / ITEM_HEIGHT_REAL;
pub const ITEM_BOX_HEIGHT: f32 =  38.0 / ITEM_HEIGHT_REAL;
pub const ITEM_BOX_TEXT_SPACE: f32 = 6.0 / ITEM_WIDTH_REAL;
pub const ICON_CHEVRON_UP: &str = "\u{E75F}";
pub const ICON_CHEVRON_DOWN: &str = "\u{E75C}";
pub const FONT_SIZE: f32 = 24.0 / ITEM_HEIGHT_REAL;
pub const NUMBER_FONT_SIZE: f32 = 26.0 / ITEM_HEIGHT_REAL;
pub const DEVICES_COUNT: i32 = 12 * 2;
pub const SPEED: f32 = 0.6; // pixel per second
pub const CORNER_RADIUS: f32 = 3.0 / ITEM_WIDTH_REAL;
pub const BORDER_THICKNESS: f32 = 1.0 / ITEM_WIDTH_REAL;



pub struct StatisticFonts<'b> {
    pub icons: Font<'b>,
    pub sans: Font<'b>,
}

pub enum RenderChange {
    NothingChanged,
    NeedRerender,
}


pub enum AnimationDirection {
    Forward,
    Backward,
}

pub enum IconColorState {
    Positiv,
    Negativ,
    Deactivated,
}

pub fn ratio(base: f32, size: f32) -> f32 {
    base * size
}