
use gl;
use glutin;

use crate::Item;

#[cfg(target_os = "linux")]
use video_gstreamer;


#[cfg(target_os = "macos")]
use video_ffmpeg;

#[cfg(target_os = "macos")]
pub struct Video {
    _gl: gl::Gl,
    inner_video: video_ffmpeg::Video,
    in_scope_state: bool,
}

#[cfg(target_os = "macos")]
impl Video {
    pub fn new(gl: &gl::Gl, _glwindow: &glutin::ContextWrapper<glutin::PossiblyCurrent, glutin::Window>,file: String) -> Video {
        let mut raw_video = video_ffmpeg::Video::new(file);
        
        let _res = raw_video.load_video(gl);
        raw_video.start_playing();
        raw_video.pause();
        Video {
            _gl: gl.clone(),
            inner_video: raw_video,
            in_scope_state: false,
        }
    }
}

#[cfg(target_os = "macos")]
impl Item for Video {

    fn update(&mut self, _dt:f32) {
        self.inner_video.update();
    }

    fn activate(&self, pos: u32) {
        if let Some(tex) = &self.inner_video.texture {
            tex.bind(pos);
        }
    }

    fn in_scope(&mut self) {
        debug!("video is in scope");
        self.in_scope_state = true;
        self.inner_video.play();
    }

    fn out_of_scope(&mut self) {
        debug!("video is out of scope");
        self.in_scope_state = false;
        self.inner_video.pause();
    }


    fn is_in_scope(&self) -> bool {
        self.in_scope_state
    } 

    fn id(&self) -> u32 {
        if let Some(tex) = &self.inner_video.texture {
            tex.id()
        } else {
            0
        }
    }


}

#[cfg(target_os = "linux")]
pub struct Video {
    gl: gl::Gl,

    inner_video: video_gstreamer::Video,

    current_texture: u32,
    //texture_buffer: 
    in_scope_state: bool,
}

#[cfg(target_os = "linux")]
impl Video {
    pub fn new(gl: &gl::Gl,glwindow: &glutin::ContextWrapper<glutin::PossiblyCurrent, glutin::Window>, file: String) -> Video {
        let mut raw_video = video_gstreamer::Video::new(file, glwindow);
        Video {
            gl: gl.clone(),
            inner_video: raw_video,
            current_texture: 0,

            in_scope_state: false,
        }
    }
}

#[cfg(target_os = "linux")]
impl Item for Video {

    fn update(&mut self, _dt:f32) {
        if let Some(new_tex) = self.inner_video.update() {
            self.current_texture = new_tex;
        }
    }

    fn activate(&self, pos: u32) {
        if self.current_texture > 0 {
            use gl_render::activate_texture;
            activate_texture(&self.gl, self.current_texture, pos);
        }
    }

    fn in_scope(&mut self) {
        self.in_scope_state = true;
        self.inner_video.play();
    }

    fn out_of_scope(&mut self) {
        self.in_scope_state = false;
        self.inner_video.pause();
    }

    fn is_in_scope(&self) -> bool {
        self.in_scope_state
    } 

    fn id(&self) -> u32 {
        self.current_texture
    }

}