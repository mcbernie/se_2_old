
use gl_render::Texture;
use client;
use crate::Item;
use std::env;

use crate::things::Video;

#[derive(Debug)]
pub enum MediaLoadingError {
    //NoImage,
    NoFile,
    //WrongFormat,
    Unknown(String),
}


pub struct Media {
    inner: client::Media,
    texture: Texture,
}

impl Media {
    pub fn load(item: client::Media, gl: &gl::Gl) -> Result<Self,MediaLoadingError> {
        let inner = item.clone();

        let mut tmp_dir = env::temp_dir();
        let fname = item.name;
        tmp_dir.push("star-entertainer-file-upload");
        tmp_dir.push(&fname);

        match tmp_dir.to_str() {
            Some(dir_str) => {
                match Texture::from_image(gl, dir_str.to_string()) {
                    Ok(texture) => {
                        Ok(Media{
                            inner,
                            texture
                        })
                    },
                    Err(e) => {
                        Err(MediaLoadingError::Unknown(format!("{:?}", e)))
                    }
                }
            },
            None => {
                Err(MediaLoadingError::NoFile)
            }
        }
    }
}

impl Item for Media {
    fn activate(&self, pos: u32) {
        self.texture.bind(pos);
    }

    fn id(&self) -> u32 {
        self.texture.id()
    }

    fn pause_time(&self) -> f32 {
        self.inner.duration as f32
    }

    fn is_in_scope(&self) -> bool {
        true
    } 
}



pub struct VideoMedia {
    inner: client::Media,
    video: Video,
}

impl VideoMedia {
    pub fn load(item: client::Media, gl: &gl::Gl, glwindow: &glutin::ContextWrapper<glutin::PossiblyCurrent, glutin::Window>) -> Result<Self,MediaLoadingError> {
        let inner = item.clone();

        let mut tmp_dir = env::temp_dir();
        let fname = item.name;
        tmp_dir.push("star-entertainer-file-upload");
        tmp_dir.push(&fname);

        match tmp_dir.to_str() {
            Some(dir_str) => {
                let video = Video::new(gl, glwindow, dir_str.to_string());
                Ok(VideoMedia{
                    inner,
                    video
                })
            },
            None => {
                Err(MediaLoadingError::NoFile)
            }
        }
    }
}

impl Item for VideoMedia {
    fn pause_time(&self) -> f32 {
        self.inner.duration as f32
    }

    fn update(&mut self, dt:f32) {
        self.video.update(dt);
    }

    fn activate(&self, pos: u32) {
       self.video.activate(pos);
    }

    fn in_scope(&mut self) {
        self.video.in_scope();
    }

    fn out_of_scope(&mut self) {
        self.video.out_of_scope();
    }


    fn is_in_scope(&self) -> bool {
        self.video.is_in_scope()
    } 

    fn id(&self) -> u32 {
        self.video.id()
    }

}