
use gl_render::Texture;
use crate::Item;

impl Item for Texture {
    fn activate(&self, pos: u32) {
        self.bind(pos);
    }

    fn id(&self) -> u32 {
        self.id()
    }

    fn is_in_scope(&self) -> bool {
        true
    } 
}