#version 150

uniform mat4 u_transformation;
in vec4 a_position;
in vec2 a_texcoord;
out vec2 v_texcoord;
void main() {
    gl_Position = u_transformation * a_position;
    //v_texcoord = a_texcoord.st * vec2(1.0, -1.0);
    v_texcoord = a_texcoord;
}