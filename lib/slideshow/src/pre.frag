#version 150 core
in vec2 v_texcoord;

out vec4 Target0;

uniform sampler2D from;
uniform sampler2D to;

uniform float progress;
uniform float flip_from;
uniform float flip_to;

vec4 getFromColor(vec2 uv) {

    return texture(from, uv * vec2(1.0, flip_from));
}

vec4 getToColor(vec2 uv) {
    return texture(to, uv * vec2(1.0, flip_to));
}
