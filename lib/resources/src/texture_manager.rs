// managing Textures

//use gl_render::Texture;
use gl::Gl;
use std::cell::RefCell;
use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::path::Path;
use std::rc::Rc;
use std::ops::Deref;

pub struct ManagedTexture{
    inner: gl_render::Texture
}

impl ManagedTexture {

    pub fn new(gl: &Gl) -> Rc<ManagedTexture> {
        let mut texture_id: gl::types::GLuint = 0;
        unsafe {
            gl.GenTextures(1, &mut texture_id);
        }

        let texture = gl_render::Texture::from_id(gl, texture_id).unwrap();
        Rc::new(ManagedTexture{inner:texture})
    }
}

impl Deref for ManagedTexture {
    type Target = gl_render::Texture;

    fn deref(&self) -> &gl_render::Texture {
        &self.inner
    }
}


thread_local!(static KEY_TEXTURE_MANAGER: RefCell<TextureManager> = RefCell::new(TextureManager::new()));

pub struct TextureManager {
    textures: HashMap<String, Rc<ManagedTexture>>,
}

impl TextureManager {
    /// Creates a new texture manager.
    pub fn new() -> TextureManager {
        TextureManager {
            textures: HashMap::new(),
        }
    }

    /// Mutably applies a function to the texture manager.
    pub fn get_global_manager<T, F: FnMut(&mut TextureManager) -> T>(mut f: F) -> T {
        KEY_TEXTURE_MANAGER.with(|manager| f(&mut *manager.borrow_mut()))
    }

    /// Get a texture with the specified name. Returns `None` if the texture is not registered.
    pub fn get(&mut self, name: &str) -> Option<Rc<ManagedTexture>> {
        self.textures.get(&name.to_string()).map(|t| t.clone())
    }

    /// Allocates a new texture that is not yet configured.
    ///
    /// If a texture with same name exists, nothing is created and the old texture is returned.
    /*pub fn add_empty(&mut self, name: &str) -> Rc<ManagedTexture> {
        match self.textures.entry(name.to_string()) {
            Entry::Occupied(entry) => entry.into_mut().0.clone(),
            Entry::Vacant(entry) => entry.insert((ManagedTexture::new(), (0, 0))).0.clone(),
        }
    }*/

    /// Allocates a new texture read from a file.
    fn load_texture_from_file(gl: &gl::Gl, path: &str) -> Rc<ManagedTexture> {

        let t = gl_render::Texture::from_image(gl, path.to_string()).unwrap();
        let texture = ManagedTexture {
            inner: t
        };

        Rc::new(texture)
    }


    /// Allocates a new texture read from a file. If a texture with same name exists, nothing is
    /// created and the old texture is returned.
    pub fn add(&mut self,gl: &gl::Gl, path: &str, name: &str) -> Rc<ManagedTexture> {
        self.textures
            .entry(name.to_string())
            .or_insert_with(|| TextureManager::load_texture_from_file(gl, path))
            .clone()
    }
}