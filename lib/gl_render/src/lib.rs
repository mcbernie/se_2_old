#[macro_use]
extern crate log;

extern crate gl;
extern crate glutin;
extern crate image;

use std::ffi::{CStr};

mod basic;
pub use basic::{clear, resize};

mod texture;
pub use texture::{Texture, activate_texture, Updateable, RawTexture};

mod shader;
pub use shader::{Program, Shader};


pub fn load_gl(gl_context: &glutin::WindowedContext<glutin::PossiblyCurrent>) -> gl::Gl {
    let gl = gl::Gl::load_with(|ptr| gl_context.get_proc_address(ptr) as *const _);

    let version = unsafe {
        let data = CStr::from_ptr(gl.GetString(gl::VERSION) as *const _)
            .to_bytes()
            .to_vec();
        String::from_utf8(data).unwrap()
    };

    info!("OpenGL version {}", version);
    gl
}
