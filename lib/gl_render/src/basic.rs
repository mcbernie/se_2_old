
use gl;
use glutin;

pub fn clear(gl: &gl::Gl) {
        unsafe {
            
            gl.ClearColor(0.0, 0.0, 0.0, 1.0);
            gl.Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT | gl::STENCIL_BUFFER_BIT);
        }
    }


pub fn resize(_gl: &gl::Gl, _size: glutin::dpi::PhysicalSize) {

}