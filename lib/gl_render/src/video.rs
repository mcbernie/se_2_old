use items::Item;
use std::time::{Instant};

pub struct Video {
    gl: gl::Gl,
    ids: Vec<gl::types::GLuint>,
    id: gl::types::GLuint,
}

impl Video {
    
    pub fn from_file(gl: &gl::Gl, file: String) -> Result<Video, String> {
    }

}

impl Drop for Video {
    fn drop(&mut self) {
    }
}


impl Item for Video {
    fn activate(&self, pos: u32) {
        self.gl.ActiveTexture(gl::TEXTURE0 + nr);
        self.gl.BindTexture(gl::TEXTURE_2D, self.id);
    }
}


