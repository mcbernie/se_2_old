use gl;
use image;
use image::{DynamicImage, GenericImageView, ImageBuffer, Rgba};



use std::ffi::c_void;
use std::time::{Instant};
use std::collections::HashMap;
use std::boxed::Box;

pub struct RawTexture {
    width: u32,
    height: u32,
    image: Vec<u8>,
}

impl RawTexture {
    pub fn from_file(file: String) -> Result<RawTexture, String> {
        match image::open(file.clone()) {
            Err(err) => panic!("Could not load image {}: {}", file, err),
            Ok(img) => {
                info!("Dimensions of image are {:?}", img.dimensions());

                let (width, height) = img.dimensions();

                let img = match img {
                    DynamicImage::ImageRgba8(img) => img,
                    img => img.to_rgba()
                };

                Ok(
                    RawTexture { 
                        image: img.into_raw(),
                        width: width,
                        height: height,  
                    }
                )
            }
        }
    }

    pub fn to_texture(&mut self, gl: &gl::Gl) -> Texture {
        Texture::new(gl, self.width, self.height, &self.image).unwrap()

    }
}

#[derive(Clone)]
pub struct Texture {
    gl: gl::Gl,
    id: gl::types::GLuint,
    width: u32, 
    height: u32,
}


#[derive(Debug)]
pub enum TextureLoadingError {
    NoImageFile,
    ImageError(image::ImageError),
}


impl Texture {
    pub fn from_image(gl: &gl::Gl, file: String) -> Result<Texture, TextureLoadingError> {

        match image::open(file.clone()) {
            Err(err) => Err(TextureLoadingError::ImageError(err)),
            Ok(img) => {
                info!("Dimensions of image are {:?}", img.dimensions());

                let (width, height) = img.dimensions();

                let img = match img {
                    DynamicImage::ImageRgba8(img) => img,
                    img => img.to_rgba()
                };
                
                Texture::new(gl, width, height, &img.into_raw())

            }
        }

    }

    pub fn from_empty(gl: &gl::Gl, width: u32, height: u32) -> Result<Texture, TextureLoadingError> {
        let image_buffer = ImageBuffer::<Rgba<u8>, Vec<u8>>::new(width, height);

        Texture::new(gl, width, height, &image_buffer.into_raw())
    }

    pub fn from_id(gl: &gl::Gl, id: u32) -> Result<Texture, String> {
        Ok(Texture {
            gl: gl.clone(),
            id: id,
            width: 1,
            height: 1,
        })
    }

    pub fn new(gl: &gl::Gl, width: u32, height: u32, image: &Vec<u8>) -> Result<Texture, TextureLoadingError> {
        
        let id = load_texture(&gl, width, height, image.to_vec())?;

        Ok(      
            Texture { 
                gl: gl.clone(), 
                id,

                width: width,
                height: height,  
            }
        )

    }

    pub fn id(&self) -> gl::types::GLuint {
        self.id
    }

    pub fn set_id(&mut self, nid: u32) {
        self.id = nid;
    }

    pub fn bind(&self, nr: u32) {
        activate_texture(&self.gl, self.id, nr);
    }

}

impl Drop for Texture {
    fn drop(&mut self) {
        unsafe {
            self.gl.DeleteTextures(1, &self.id);
        }
    }
}


pub trait Updateable {
    fn update_from_buffer(&mut self, buffer: Vec<u8> );
    //fn update_from_file(&self, )
}

impl Updateable for Texture {
    fn update_from_buffer(&mut self, buffer: Vec<u8> ) {
        update_texture(&self.gl, self.id, self.width, self.height, buffer)
    }
}

fn load_texture(
    gl: &gl::Gl, 
    width: u32, 
    height: u32, 
    image: Vec<u8>
) -> Result<gl::types::GLuint, TextureLoadingError> {
    
    let mut texture_id: gl::types::GLuint = 0;
    unsafe {
        gl.GenTextures(1, &mut texture_id);
        gl.BindTexture(gl::TEXTURE_2D, texture_id);
    }

    let img_ptr = image.as_ptr() as *const c_void;

    let now = Instant::now();
    unsafe{
        gl.TexImage2D(gl::TEXTURE_2D,
                    0,
                    gl::RGBA as i32,
                    width as i32,
                    height as i32,
                    0,
                    gl::RGBA,
                    gl::UNSIGNED_BYTE,
                    img_ptr);
        gl.GenerateMipmap(gl::TEXTURE_2D);
        gl.TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR as i32);
        gl.TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);
        debug!("time for save image to gpu texture: {}ms", now.elapsed().as_millis());
        gl.BindTexture(gl::TEXTURE_2D, 0);
    }

    Ok(texture_id)
}

fn update_texture(gl: &gl::Gl, id: u32, width: u32, height: u32, image: Vec<u8>) {
    unsafe {
        gl.BindTexture(gl::TEXTURE_2D, id);
    }

    let img_ptr = image.as_ptr() as *const c_void;
    
    unsafe{
        gl.TexImage2D(gl::TEXTURE_2D,
                    0,
                    gl::RGBA as i32,
                    width as i32,
                    height as i32,
                    0,
                    gl::RGBA,
                    gl::UNSIGNED_BYTE,
                    img_ptr);
        gl.GenerateMipmap(gl::TEXTURE_2D);
        gl.TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::NEAREST as i32);
        gl.TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::NEAREST as i32);
        
        gl.BindTexture(gl::TEXTURE_2D, 0);
    }

}


pub fn activate_texture(gl: &gl::Gl, id: u32, pos: u32) {
    unsafe {
        gl.ActiveTexture(gl::TEXTURE0 + pos);
        gl.BindTexture(gl::TEXTURE_2D, id);
    }
}